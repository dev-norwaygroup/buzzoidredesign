module.exports = {
  locales: ['en', 'fr', 'de', 'nl', 'pt'],
  sourceLocale: 'en',
  fallbackLocales: {
    default: 'en',
  },
  catalogs: [
    {
      path: 'translations/locales/{locale}/messages',
      include: ['components', 'pages', 'statics', 'blocks'],
    },
  ],
  format: 'po',
};
