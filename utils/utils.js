/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';
import { en, fr, de, nl, pt } from 'make-plural/plurals';

/**
 * Internal dependencies
 */
import siteMap from 'statics/site-map';

export function initTranslation(i18n) {
  i18n.loadLocaleData({
    en: { plurals: en },
    fr: { plurals: fr },
    de: { plurals: de },
    nl: { plurals: nl },
    pt: { plurals: pt },
  });
}

export async function loadTranslation(locale, isProduction = true) {
  let data;
  if (isProduction) {
    data = await import(`../translations/locales/${locale}/messages`);
  } else {
    data = await import(
      `@lingui/loader!../translations/locales/${locale}/messages.po`
    );
  }

  return data.messages;
}

export const getCategoryDetails = (category, index) => {
  const isMultipleOfThree = index % 3 === 0;
  const isMultipleOfTwo = index % 2 === 0;
  let color, text;

  if (isMultipleOfThree) {
    color = 'orange';
  } else if (isMultipleOfTwo) {
    color = 'yellow';
  } else {
    color = 'blue';
  }

  if (category == 4) {
    text = <Trans>Instagram Followers</Trans>;
  } else if (category == 124) {
    text = <Trans>Instagram Analytics</Trans>;
  } else {
    text = <Trans>Instagram Marketing</Trans>;
  }

  return {
    color,
    text,
  };
};

export const onScrollToTop = (isSmooth) => {
  if (typeof window === 'object') {
    window.scrollTo({ top: 0, behavior: isSmooth ? 'smooth' : 'instant' });
  }
};

export const getTranslatedPath = (path) => {
  const enObj =  Object
    .values(siteMap)
    .find(obj => Object.entries(obj).find(([_, value]) => value == path));

  if (! enObj) {
    return;
  }
  return Object.entries(enObj).find(([_, value]) => value == path)[0];
}

export const isLocalePathMatching = (path, locale) => Object.entries(siteMap[locale]).find(([_, value]) => value == path);