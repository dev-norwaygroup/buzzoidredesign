const Animate = ({ children }) => (
  <div className="animate" data-aos="fade-up">
    {children}
  </div>
);

export default Animate;
