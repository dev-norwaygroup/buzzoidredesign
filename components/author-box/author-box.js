import classNames from 'classnames';
import Image from 'next/image';

import VerfiedOrange from '@/svg/icon-verified-orange.svg';

const AuthorBox = ({ children, avatar, small, name, verified, align }) => {
  const size = small ? 36 : 46;
  return (
    <div className={classNames('author-box', { [`align-${align}`]: align })}>
      <div className="author-box__col">
        <div className="author-box__image">
          <Image src={avatar} width={size} height={size} alt="Avatar Image"/>
        </div>
      </div>
      <div className="author-box__col">
        <h5>
          {name}
          {verified && <VerfiedOrange />}
        </h5>
        {children}
      </div>
    </div>
  );
};

export default AuthorBox;
