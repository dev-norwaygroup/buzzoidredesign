/**
 * External Dependencies
 */
import classNames from "classnames"

/**
 * Internal Dependencies
 */
import TranslatedLink from "../translated-link/translated-link"

const Button = ({ children, href, className, onClick }) => (
  <TranslatedLink href={href}>
    <a onClick={onClick} className={classNames('btn', className)}>
      {children}
    </a>
  </TranslatedLink>
)

export default Button
