/**
 * External Dependencies
 */
import classNames from 'classnames';
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import IconChevronRight from '@/svg/icon-chevron-right.svg';
import TranslatedLink from '../translated-link/translated-link';

const FancyButton = ({ href, heading, isMostPopular, price }) => {
  return (
    <TranslatedLink href={href}>
      <a className={classNames('fancy-btn', { 'is-popular': isMostPopular })}>
        <div className="fancy-btn__entry">
          { isMostPopular && <span className="highlight"><Trans>Most Popular</Trans></span> }
          <h5>{heading}</h5>
          <h6>
            <Trans>Starting at</Trans>
            <span>{price}</span>
          </h6>
        </div>
        <div className="fancy-btn__arrow">
          <IconChevronRight />
        </div>
      </a>
    </TranslatedLink>
  );
};

export default FancyButton;
