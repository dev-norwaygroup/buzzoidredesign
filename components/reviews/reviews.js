/**
 * External Dependencies
 */
import { useCallback, useState, useEffect } from 'react';
import useEmblaCarousel from 'embla-carousel-react';
import classNames from 'classnames';
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import { PrevButton, NextButton, DotButton } from './embla-buttons';
import Review from './review';
import ReviewForm from 'blocks/review-form/review-form';

const Reviews = ({
  data
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [scrollSnaps, setScrollSnaps] = useState([]);

  const [emblaRef, embla] = useEmblaCarousel({
    align: 'start',
    loop: true,
    skipSnaps: false,
    inViewThreshold: 0.7,
  });

  const toggleIsOpen = () => setIsOpen(!isOpen);
  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const scrollTo = useCallback(
    (index) => embla && embla.scrollTo(index),
    [embla]
  );

  const onSelect = useCallback(() => {
    if (!embla) return;
    setSelectedIndex(embla.selectedScrollSnap());
  }, [embla, setSelectedIndex]);

  useEffect(() => {
    if (!embla) return;
    onSelect();
    setScrollSnaps(embla.scrollSnapList());
    embla.on('select', onSelect);
  }, [embla, setScrollSnaps, onSelect]);

  return (
    <div className={classNames('reviews', { 'is-open': isOpen })}>
      <div className="shell">
        <div className="embla" ref={emblaRef}>
          <div className="embla__container">
            {data.map((review, index) => (
              <div className="embla__slide" key={index}>
                <Review stars={review.stars ? review.stars : 5} imgSrc={review.imgSrc} text={review.text} author={review.author} />
              </div>
            ))}
          </div>
        </div>
        <div className="embla__actions">
          <PrevButton onClick={scrollPrev} />
          <NextButton onClick={scrollNext} />

          <div className="embla__dots">
            {scrollSnaps.map((_, index) => (
              <DotButton
                key={index}
                selected={index === selectedIndex}
                onClick={() => scrollTo(index)}
              />
            ))}
          </div>
        </div>
        <div className="reviews__actions">
          <div className="reviews__submit" onClick={toggleIsOpen}>
            <span className="reviews__btn-text">
              {isOpen ? '' : <Trans>Submit Your Review</Trans>}
            </span>
          </div>
          <ReviewForm isOpen={isOpen} />
        </div>
      </div>
    </div>
  );
};

export default Reviews;
