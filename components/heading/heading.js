/**
 * External Dependencies
 */
import classNames from 'classnames';

const Heading = ({ children, className }) => (
  <div className={classNames('heading', className)}>
    <div className="shell">
      <div className="hеading__inner">{children}</div>
    </div>
  </div>
);

export default Heading;
