/**
 * External Dependencies
 */
import classNames from 'classnames';

const AccordionItem = ({ title, content, onToggle, isActive }) => (
  <div
    className={classNames('accordion__item has-border-top', {
      'is-open': isActive,
    })}
  >
    <div className="accordion__head" onClick={onToggle}>
      <div className="accordion__question-mark">
        <span>?</span>
      </div>
      <div className="accordion__title">{title}</div>
      <div className="accordion__icon-wrapper">
        <span className="accordion__icon">{isActive ? '-' : '+'}</span>
      </div>
    </div>
    <div className="accordion__content">{content}</div>
  </div>
);

export default AccordionItem;
