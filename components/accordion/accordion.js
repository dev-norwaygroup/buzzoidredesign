/**
 * External Dependencies
 */
import { useState } from "react";

/**
* Internal Dependencies
*/
import AccordionItem from "./accordion-item";

const Accordion = ({
    items
}) => {
    const [clicked, setClicked] = useState(null);

    const handleToggle = (index) => {
        if (clicked === index) {
            return setClicked(null);
        }
        setClicked(index);
    };

    return (
        <div className="accordion has-border">
            {
                items?.map((x, index) => {
                    return <AccordionItem
                        key={index}
                        title={x.title}
                        content={x.content}
                        onToggle={() => handleToggle(index)}
                        isActive={clicked === index}
                    />
                })
            }
        </div>
    )
}

export default Accordion;
