/**
 * External Dependencies
 */
import Link from 'next/link';
import classNames from 'classnames';
import { useRouter } from 'next/router';

/**
 * Internal Dependencies
 */
import TranslatedLink from '../translated-link/translated-link';
import siteMap from 'statics/site-map';

const Nav = ({ items, heading, className, currentCategory, isLocalized = true }) => {
  const router = useRouter();
  const locale = router.locale;

  return (
    <nav className={classNames('nav', className)}>
      {heading && <h3>{heading}</h3>}
      <ul>
        {items?.map((item) => (
          <li
            key={item.href}
            className={classNames({
              active:
                router.asPath === item.href ||
                router.asPath === `/${locale}/${item.href}` ||
                router.asPath === siteMap[locale]?.[item.href] ||
                currentCategory == item.href.split('/').splice(-2)[0]
            })}
          >
            {
              isLocalized
                ? (
                  <TranslatedLink href={item.href} isEn={!!item.isEn}>
                    <a>{item.content}</a>
                  </TranslatedLink>
                )
                : (
                  <Link href={item.href} locale='en'>
                    <a>{item.content}</a>
                  </Link>
                )
            }
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Nav;
