/**
 * External Dependencies
 */
import Link from 'next/link';
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import Button from '../button/button';

const NavAccess = () => (
  <nav className="nav nav-access">
    <ul>
      <li>
        <Link href="/login/">
          <a>
            <Trans>Login</Trans>
          </a>
        </Link>
      </li>
      <li>
        <Button href="/signup/" className="btn--white">
          <Trans>Sign Up</Trans>
        </Button>
      </li>
    </ul>
  </nav>
);

export default NavAccess;
