import SVGLike from './svg/svg-like';
import SVGFollowers from './svg/svg-followers';
import SVGViews from './svg/svg-views';
import SVGAutomatic from './svg/svg-automatic';
import SVGHome from './svg/svg-home';

const CustomSVG = ({ type }) => {
  switch (type) {
    case 'like':
      return <SVGLike />;
    case 'followers':
      return <SVGFollowers />;
    case 'automatic':
      return <SVGAutomatic />;
    case 'home':
      return <SVGHome />;
    case 'views':
      return <SVGViews />;
    default:
      return <></>;
  }
};

export default CustomSVG;
