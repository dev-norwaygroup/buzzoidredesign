/**
 * External Dependencies
 */
import classNames from 'classnames';

const IconText = ({ isRow, icon, text }) => (
  <div className={classNames('icon-text', { 'is-row': isRow })}>
    <div className="icon-text__img">
      {icon}
    </div>
    <div className="icon-text__text ">{text}</div>
  </div>
);

export default IconText;
