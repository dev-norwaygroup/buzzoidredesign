/**
 * External Dependencies
 */
import Link from "next/link";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import classNames from "classnames";

/**
 * Internal Dependencies
 */
import NavAccess from "@/components/nav/nav-access";
import Nav from '@/components/nav/nav';
import LogoBuzzoid from '@/svg/logo-buzzoid.svg';
import IconChevronRightGray from '@/svg/icon-chevron-right-gray.svg';
import Button from "../button/button";
import navItems from "./nav-items";
import { Trans } from "@lingui/macro";
import TranslatedLink from "../translated-link/translated-link";

const Header = () => {
    const [isOpen, setIsOpen] = useState(false);
    const router = useRouter();

    const handleOutClick = () => {
        if (isOpen) {
            setIsOpen(false);
        }
    };

    useEffect(() => setIsOpen(false), [router.asPath]);

    useEffect(() => {
        if (typeof window === 'object') {
          window.addEventListener('click', handleOutClick);
        }
        return () => window.removeEventListener('click', handleOutClick);
      });

    return (
        <header className="header">
            <div className="shell">
                <div className="header__inner">
                    <Link href="/" passHref>
                        <a className="header__logo" onClick={() => setIsOpen(false)}>
                            <LogoBuzzoid />
                        </a>
                    </Link>

                    <Nav items={navItems} className="nav-main" />

                    <div className="nav__outer">
                        <NavAccess />
                    </div>
                    <div className="burger-icon" onClick={() => setIsOpen(!isOpen)}>
                        <span className={classNames('burger-icon__inner', { 'is-open': isOpen })} ></span>
                    </div>
                </div>
                <div className={classNames('header__mobile', { 'is-open': isOpen })}>
                    <div className="header__access has-border">
                        <TranslatedLink href="/">
                            <a onClick={() => setIsOpen(false)}><Trans>Already a Buzzoider?</Trans></a>
                        </TranslatedLink>
                        <Button href="/signin" className="btn--orange" onClick={() => setIsOpen(!isOpen)}><Trans>Sign In</Trans></Button>
                    </div>
                    <div className="header__main has-border">
                        <nav>
                            <ul>
                                <li className="auto">
                                    <TranslatedLink href="/automatic-instagram-likes/">
                                        <a onClick={() => setIsOpen(false)}>
                                            <Trans>Buy Automatic Likes</Trans>
                                            <div className="group">
                                                <span><Trans>New</Trans></span>
                                                <IconChevronRightGray />
                                            </div>
                                        </a>
                                    </TranslatedLink>
                                </li>
                                <li className="likes">
                                    <TranslatedLink href="/buy-instagram-likes/">
                                        <a onClick={() => setIsOpen(false)}><Trans>Buy Instagram Likes</Trans><IconChevronRightGray /></a>
                                    </TranslatedLink>
                                </li>
                                <li className="followers">
                                    <TranslatedLink href="/buy-instagram-followers/">
                                        <a onClick={() => setIsOpen(false)}><Trans>Buy Instagram Followers</Trans><IconChevronRightGray /></a>
                                    </TranslatedLink>
                                </li>
                                <li className="views">
                                    <TranslatedLink href="/buy-instagram-views/">
                                        <a onClick={() => setIsOpen(false)}><Trans>Buy Instagram Views</Trans><IconChevronRightGray /></a>
                                    </TranslatedLink>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div className="header__footer">
                        <TranslatedLink href="/faq/"><a onClick={() => setIsOpen(false)}><Trans>FAQ</Trans></a></TranslatedLink>
                        <TranslatedLink href="/contact-us/"><a onClick={() => setIsOpen(false)}><Trans>Contact Us</Trans></a></TranslatedLink>
                        <Link href="/privacy-policy/" passHref locale='en'><a onClick={() => setIsOpen(false)}><Trans>Privacy Policy</Trans></a></Link>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header;
