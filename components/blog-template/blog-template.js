import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import Animate from '@/components/animate/animate';
import Offers from '@/components/offers/offers';
import CustomerRated from '@/components/customer-rated/customer-rated';
import FancyButtonGroup from '@/components/button/fancy-button-group';
import PostHead from '@/components/post-head/post-head';
import PostsGroup from '@/components/posts-group/posts-group';
import BlogNavigation from '@/components/blog-navigation/blog-navigation';
import Pagination from '@/components/pagination/pagination';
import FancyButton from '../button/fancy-button';
import { onScrollToTop } from 'utils/utils';

const BlogTemplate = ({
  posts,
  routerActive = true,
  currentPage,
  setCurrentPage,
  avaiablePages,
  checkCategory,
}) => {
  const onArrowBackClick = () => {
    if (currentPage - 1 >= 1) {
      setCurrentPage(currentPage - 1);
      onScrollToTop();
    }
  };

  const onArrowForwardClick = () => {
    if (currentPage + 1 <= avaiablePages) {
      setCurrentPage(currentPage + 1);
      onScrollToTop();
    }
  };

  const onPageClick = (index) => {
    setCurrentPage(index);
    onScrollToTop();
  }

  return (
    <div className="blog">
      <Animate>
        <BlogNavigation
          routerActive={routerActive}
          checkCategory={checkCategory}
        />
        <div className="blog__intro has-border">
          <div className="shell">
            <PostHead post={posts[0]} />
          </div>
        </div>
      </Animate>
      <Animate>
        <PostsGroup seekForFirst={true} title="Latest Posts" posts={posts} />
      </Animate>
      <Animate>
        <Pagination
          pages={avaiablePages}
          onPageClick={onPageClick}
          onArrowBackClick={onArrowBackClick}
          onArrowForwardClick={onArrowForwardClick}
          currentPage={currentPage}
        />
      </Animate>
      <Animate>
        <Offers>
          <FancyButtonGroup>
            <FancyButton
              href="/buy-instagram-followers/"
              price="$2.97"
              isMostPopular={true}
              heading={<Trans>Buy Followers</Trans>}
            />
            <FancyButton href="/buy-instagram-likes/" heading={<Trans>Buy Likes</Trans>} price="$1.47" />
            <FancyButton href="/buy-instagram-views/" heading={<Trans>Buy Views</Trans>} price="$1.99" />
          </FancyButtonGroup>
          <CustomerRated />
        </Offers>
      </Animate>
    </div>
  );
};

export default BlogTemplate;
