/**
 * External Dependencies
 */
import Link from 'next/link';
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import Nav from '@/components/nav/nav';
import Dropdown from '@/components/dropdown/dropdown';
import { buzzItems, servicesItems, aboutUsItems } from './nav-items';
import Payments from './payments';
import IconChevronUp from '@/svg/icon-chevron-up.svg';
import LogoBuzzoid from '@/svg/logo-buzzoid.svg';
import LogoDutch from '@/svg/logo-dutch.svg';
import LogoGermany from '@/svg/logo-germany.svg';
import LogoPortuguese from '@/svg/logo-portuguese.svg';
import LogoFrance from '@/svg/logo-france.svg';
import IconGlobe from '@/svg/icon-globe.svg';
import { onScrollToTop } from 'utils/utils';
import TranslatedLink from '../translated-link/translated-link';

const dropdownItems = [
  {
    label: (
      <>
        <IconGlobe />
        <Trans>English</Trans>
      </>
    ),
    value: 'en',
  },
  {
    label: (
      <>
        <LogoGermany />
        <Trans>German</Trans>
      </>
    ),
    value: 'de',
  },
  {
    label: (
      <>
        <LogoFrance />
        <Trans>French</Trans>
      </>
    ),
    value: 'fr',
  },
  {
    label: (
      <>
        <LogoPortuguese />
        <Trans>Portuguese</Trans>
      </>
    ),
    value: 'pt',
  },
  {
    label: (
      <>
        <LogoDutch />
        <Trans>Dutch</Trans>
      </>
    ),
    value: 'nl',
  },
];

const Footer = () => {
  const currentYear = new Date().getFullYear();

  return (
    <footer className="footer has-border-top">
      <div className="shell">
        <div className="footer__inner">
          <div className="footer__nav">
            <Link href="/">
              <a className="footer__logo">
                <LogoBuzzoid />
              </a>
            </Link>
            <Nav
              className="nav-secondary services"
              heading={<Trans>Our Services</Trans>}
              items={servicesItems}
            />

            <Nav
              className="nav-secondary about"
              heading={<Trans>About Us</Trans>}
              items={aboutUsItems}
            />

            <Nav
              className="nav-secondary buzz"
              heading={<Trans>My Buzz</Trans>}
              items={buzzItems}
            />
          </div>

          <div className="footer__extra">
            <p className="copyright">
              <Trans>Copyright</Trans> &copy; {currentYear} <Trans>Buzzoid. All Rights Reserved</Trans>
            </p>
            <div className="footer__utils">
              <TranslatedLink href="/contact-us/">
                <a>
                  <Trans>Contact Us</Trans>
                </a>
              </TranslatedLink>
              <Link href="/terms-of-service" locale='en'>
                <a>
                  <Trans>Terms of Service</Trans>
                </a>
              </Link>
              <Link href="/privacy-policy" locale='en'>
                <a>
                  <Trans>Privacy Policy</Trans>
                </a>
              </Link>
            </div>
            <Payments />
          </div>
        </div>
      </div>
      <div className="divider"></div>
      <div className="shell">
        <div className="footer__outro">
          <Dropdown
            className="show-top"
            defaultComponent={
              <>
                <IconGlobe />
                <Trans>English</Trans>
              </>
            }
            items={dropdownItems}
          />
          <div className="btn-scroll" onClick={() => onScrollToTop(true)}>
            <IconChevronUp />
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
