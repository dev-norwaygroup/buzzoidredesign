/**
 * External dependencies
 */
import Link from "next/link";
import { useRouter } from 'next/router';

/**
 * Internal dependencies
 */
import siteMap from "statics/site-map";

const TranslatedLink = ({ href, children, isEn = false }) => {
    const { locale } = useRouter()
    const translatedPath = siteMap[locale]?.[href];
    let as;

    if (translatedPath && locale != 'en') {
        as = `/${locale}${translatedPath}`;
    }

    return (
        isEn
            ? (
                <Link href={href} locale='en'>
                    {children}
                </Link>
            )
            : (
                <Link href={href} as={as}>
                    {children}
                </Link>
            )
    )
}

export default TranslatedLink;
