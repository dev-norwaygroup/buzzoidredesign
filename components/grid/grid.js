import classNames from 'classnames';

const Grid = ({ children, grid, className, fullwidth, isUl }) => (
  <div className="grid">
    <div className={classNames('shell', { [`shell--fullwidth`]: fullwidth })}>
      {
        isUl
          ? (
            <ul className={classNames('grid__inner', `grid__inner--${grid}`, className)}>
              {children}
            </ul>
          )

          : (<div
            className={classNames('grid__inner', `grid__inner--${grid}`, className)}>
            {children}
          </div>
          )
      }
    </div>
  </div>
);

export default Grid;
