/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import Button from "@/components/button/button";
import IconFigures from '@/svg/icon-figures.svg';

const Callout = () => {
    return (
        <div className='callout'>
            <div className="shell">
                <div className="callout__inner">
                    <span>
                        <Trans>New! Automatic Instagram Likes!</Trans>
                    </span>
                    <Button href="/automatic-instagram-likes/" className="btn--white"><Trans>More</Trans></Button>
                </div>
            </div>
            <div className="callout__svg">
                <IconFigures />
            </div>
        </div>
    )
}

export default Callout;
