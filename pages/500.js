/**
 * External dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';

const Custom500 = () => {
    return (
        <div className="error-page">
            <div className="shell">
                <div className="error-page__inner">
                    <h1><Trans>Internal sever error</Trans></h1>
                    <p><Trans>An error has occurred and we&apos;re working to fix the problem!</Trans></p>
                    <p><Trans>We&apos;ll be up and running shortly.</Trans></p>
                </div>
            </div>
        </div>
    )
};

export default Custom500;


export const getStaticProps = async (ctx) => {
    const translation = await loadTranslation(
        ctx.locale,
        process.env.NODE_ENV === 'production'
    );

    return {
        props: {
            translation,
        },
    };
};