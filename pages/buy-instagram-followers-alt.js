/**
 * External Dependencies
 */
import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';
import { Trans } from '@lingui/macro';
import * as yup from 'yup';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Animate from '@/components/animate/animate';
import Logos from '@/components/logos/logos';
import Heading from '@/components/heading/heading';
import BuyForm from '@/blocks/buy-form/buy-form';
import { buyFollowersItems } from 'statics/faq';
import { ReviewsFollowersSchema } from 'statics/reviews';
import { followersIcons } from 'statics/icons';
import Offers from '@/components/offers/offers';
import { followersSchema } from 'statics/faq-schemas';

import Grid from '@/components/grid/grid';
import BoxAnimate from '@/components/box-animate/box-animate';
import Faqs from '@/components/faqs/faqs';
import FlexRow from '@/components/flex-row/flex-row';
import DataFollowers from '@/blocks/buy-form/data/data-followers';
import CustomerStories from '@/components/customer-stories/customer-stories';
import Reviews from '@/components/reviews/reviews';
import { followersReviews } from 'statics/reviews-temp';

import AnimateDelivery from '@/svg/animate-delivery.svg';
import AnimateGurantee from '@/svg/animate-guarantee.svg';
import AnimateSupport from '@/svg/animate-support.svg';

import IconLock from '@/svg/icon-lock.svg';
import IconLighting from '@/svg/icon-lighting.svg';
import IconShield from '@/svg/icon-shield.svg';
import IconUsers from '@/svg/icon-users.svg';
import IconSupport from '@/svg/icon-support.svg';
import IconChevronRight from '@/svg/icon-chevron-right-gray.svg';

import IntroImage from '../assets/images/intro-static4.png';

const schema = yup.object().shape({
  type: yup.string().required(),
  followers: yup.string().when('type', {
    is: 'High Quality',
    then: yup
      .string()
      .nullable()
      .required('Please choose a number of followers'),
  }),
  followers_active: yup.string().when('type', {
    is: 'Active Followers',
    then: yup
      .string()
      .nullable()
      .required('Please choose a number of followers'),
  }),
});

const BuyFollowers = () => (
  <>
    <Head>
      <meta name="twitter:image" content="/buy-instagram-followers.png" />
      <meta property="og:image:secure_url" content="/buy-instagram-followers.png" />
      <meta property="og:image" content="/buy-instagram-followers.png" />
      <link rel="alternate" hrefLang="en" href="https://buzzoid.com/buy-instagram-followers-alt/" />
      <link rel="alternate" hrefLang="fr" href="https://buzzoid.com/fr/acheter-instagram-followers-alt/" />
      <link rel="alternate" hrefLang="de" href="https://buzzoid.com/de/instagram-followers-kaufen-alt/" />
      <link rel="alternate" hrefLang="pt" href="https://buzzoid.com/pt/comprar-seguidores-instagram-alt/" />
      <link rel="alternate" hrefLang="nl" href="https://buzzoid.com/nl/instagram-followers-kopen-alt/" />
    </Head>
    <div className="intro">
      <div className="shell">
        <div className="intro__inner">
          <div className="intro__entry smaller">
            <div className="sponsored">
              <FlexRow>
                <FlexRow.Col>
                  <div className="sponsored__text">
                    <Trans>SPONSORED</Trans>
                  </div>
                </FlexRow.Col>
                <FlexRow.Col>
                  <Link href="https://growthoid.com/?utm_source=Buzzoid&utm_medium=Followers" passHref>
                    <a target="_blank">
                      <Trans>Managed Growth</Trans> <IconChevronRight />
                    </a>
                  </Link>
                </FlexRow.Col>
              </FlexRow>
            </div>
            <h1>
              <Trans>Buy Instagram Followers with Instant Delivery</Trans>
            </h1>
            <p className="fullwidth">
              <Trans>
                At Buzzoid, you can buy Instagram followers quickly, safely
                and easily with just a few clicks. See our deals below!
              </Trans>
            </p>
            <BuyForm fields={DataFollowers} schema={schema} />
            <FlexRow grid="2" className="form-icons-grid">
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconShield width="18" height="17" />
                </div>
                <Trans>High Quality Followers</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconLock />
                </div>
                <Trans>No password required</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconUsers />
                </div>
                <Trans>Real Active Followers</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconLighting />
                </div>
                <Trans>Fast delivery</Trans> (<Trans>gradual or instant</Trans>)
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconSupport />
                </div>
                24/7 <Trans>Live Support</Trans>
              </FlexRow.Col>
            </FlexRow>
          </div>
          <div className="intro__img">
            <Image
              src={IntroImage}
              width="580"
              height="604"
              alt="intro image"
            />
          </div>
        </div>
      </div>
    </div>
    <Animate>
      <Logos />
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Ready to Buy Instagram Followers?</Trans>
        </h2>
        <p>
          <Trans>
            Followers are a fundamental part of your Instagram success. Buy Instagram followers from Buzzoid, and watch your Instagram profile gain more recognition, visibility and exposure.
          </Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <Grid grid="3" isUl={true}>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateDelivery className="animate-delivery" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Instant Delivery Guaranteed</Trans>
            </h4>
            <p>
              <Trans>
                Don&apos;t wait to get your followers. Orders typically process within minutes of purchase.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateGurantee className="animate-gurantee" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>High Quality Followers</Trans>
            </h4>
            <p>
              <Trans>
                We offer the best quality followers the market has to offer. No fake Instagram followers!
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateSupport className="animate-support" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>24/7 Customer Support</Trans>
            </h4>
            <p>
              <Trans>
                Nothing worse than dealing with bad customer support. We got your back.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
      </Grid>
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Buy Instagram Followers Easily With Buzzoid</Trans>
        </h2>
        <p className="fullwidth">
          <Trans>Over 10,000 daily customers trust us as the best site to deliver real Instagram followers</Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <div className="shell">
        <Faqs items={buyFollowersItems} />
      </div>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(followersSchema),
        }}
      ></script>
    </Animate>
    <div className="divider divider--mb-90"></div>
    <CustomerStories />
    <Animate>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(ReviewsFollowersSchema),
        }}
      ></script>
      <Reviews data={followersReviews} />
    </Animate>
    <Animate>
      <Offers icons={followersIcons}>
        <BuyForm fields={DataFollowers} schema={schema} prefix="offers" />
      </Offers>
    </Animate>
  </>
);

export default BuyFollowers;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};
