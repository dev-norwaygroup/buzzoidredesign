/**
 * External dependencies
 */
import { Trans } from '@lingui/macro';
import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Heading from '@/components/heading/heading';
import Logos from '@/components/logos/logos';
import BoxRow from '@/components/box-row/box-row';
import Grid from '@/components/grid/grid';
import BoxAnimate from '@/components/box-animate/box-animate';
import Animate from '@/components/animate/animate';
import FancyButtonGroup from '@/components/button/fancy-button-group';
import CustomerRated from '@/components/customer-rated/customer-rated';
import Button from '@/components/button/button';
import CustomerStories from '@/components/customer-stories/customer-stories';
import Reviews from '@/components/reviews/reviews';
import Offers from '@/components/offers/offers';
import FancyButton from '@/components/button/fancy-button';
import { homeReviews } from 'statics/reviews-temp';
import TranslatedLink from '@/components/translated-link/translated-link';

import AnimateActive from '@/svg/animate-active.svg';
import AnimateBuy from '@/svg/animate-buy.svg';
import AnimateSelectPackage from '@/svg/animate-select-package.svg';
import AnimateDetails from '@/svg/animate-details.svg';
import AnimateMagic from '@/svg/animate-magic.svg';

import AnimateDelivery from '@/svg/animate-delivery.svg';
import AnimateGurantee from '@/svg/animate-guarantee.svg';
import AnimateSupport from '@/svg/animate-support.svg';

import ArrowDown1 from '@/svg/arrow-down1.svg';
import ArrowDown2 from '@/svg/arrow-down2.svg';

import IntroImage from '/assets/images/intro-static1.png';

const Home = () => {
  let isMobile = false;

  if (typeof window !== 'undefined') {
    if (window.matchMedia('(max-width: 767px)').matches) {
      isMobile = true;
    }
  }

  return (
    <>
      <Head>
        <meta name="twitter:image" content="../assets/images/logo-icon.png" />
        <meta property="og:image:secure_url" content="../assets/images/logo-icon.png" />
        <meta property="og:image" content="../assets/images/logo-icon.png" />
        <link rel="alternate" hrefLang="en" href="https://buzzoid.com/" />
        <link rel="alternate" hrefLang="fr" href="https://buzzoid.com/fr/" />
        <link rel="alternate" hrefLang="de" href="https://buzzoid.com/de/" />
        <link rel="alternate" hrefLang="pt" href="https://buzzoid.com/pt/" />
        <link rel="alternate" hrefLang="nl" href="https://buzzoid.com/nl/" />
      </Head>
      <Animate>
        <div className="intro">
          <div className="shell">
            <div className="intro__inner">
              <div className="intro__entry">
                <h1>
                  <Trans>
                    Buy Instagram Followers and Likes Delivered in Minutes
                  </Trans>
                </h1>
                <p>
                  <Trans>
                    Social media is exploding and a revolution is going on
                    that&apos;s changing the way consumers interact with
                    businesses.
                  </Trans>
                </p>
                <FancyButtonGroup isRow>
                  <FancyButton
                    href="/buy-instagram-followers/"
                    price="$2.97"
                    isMostPopular={true}
                    heading={<Trans>Buy Followers</Trans>}
                  />
                  <FancyButton
                    href="/buy-instagram-likes/"
                    price="$1.47"
                    heading={<Trans>Buy Likes</Trans>}
                  />
                  <FancyButton
                    href="/buy-instagram-views/"
                    price="$1.99"
                    heading={<Trans>Buy Views</Trans>}
                  />
                </FancyButtonGroup>
                <CustomerRated />
              </div>
              <div className="intro__img">
                <Image
                  src={IntroImage}
                  width={509}
                  height="394"
                  alt="intro image"
                />
              </div>
            </div>
          </div>
        </div>
      </Animate>
      <Animate>
        <Logos />
      </Animate>
      <Animate>
        <Heading className="ta-c">
          <h2>
            <Trans>Our process</Trans>
          </h2>
          <p>
            <Trans>
              Most individuals are totally unwilling to invest time into a
              profile that has little interaction.
            </Trans>
          </p>
        </Heading>
      </Animate>
      <Animate>
        <BoxRow className="align-items-center">
          <BoxRow.Col style={{ flexBasis: '36%' }}>
            <AnimateActive className="animate-active" />
          </BoxRow.Col>
          <BoxRow.Col style={{ flexBasis: '54%' }}>
            <p>
              <Trans>
                However, if they see other members in their social network</Trans>{' '}
                <strong><Trans>actively participating</Trans></strong>, <Trans>it makes investing time
                into a social media profile both fun and easy.</Trans>
            </p>
          </BoxRow.Col>
        </BoxRow>
      </Animate>
      <Animate>
        <BoxRow className="align-items-center js-animate-opacity box-row-animate-buy">
          <BoxRow.Col style={{ flexBasis: '54%' }}>
            <div className="arrow-down arrow-down--right">
              <ArrowDown1 />
            </div>
            <p>
              <strong><Trans>Buzzoid is here to help!</Trans></strong>{' '}
              <TranslatedLink href="/buy-instagram-followers/"><a><Trans>Buy Instagram followers</Trans></a></TranslatedLink>,{' '}
              <TranslatedLink href="/buy-instagram-likes/"><a><Trans>likes</Trans></a></TranslatedLink> <Trans>and </Trans>{' '}<TranslatedLink href="/buy-instagram-views/"><a><Trans>views</Trans></a></TranslatedLink> <Trans>that
                are delivered instantly and safely</Trans>.

            </p>
          </BoxRow.Col>
          <BoxRow.Col style={{ flexBasis: '46.2%' }}>
            <AnimateBuy className="animate-buy" />
          </BoxRow.Col>
        </BoxRow>
      </Animate>
      <Animate>
        <BoxRow className="js-animate-opacity box-row-select-package">
          <BoxRow.Col style={{ flexBasis: '44%' }}>
            <AnimateSelectPackage className="animate-select-package" />
          </BoxRow.Col>
          <BoxRow.Col style={{ flexBasis: '54%' }}>
            <h4>
              <Trans>Select package</Trans> <i className="icon-checkmark"></i>
            </h4>
            <p>
              <Trans>
                We offer a variety of different packages to give you the best
                value for Instagram Likes and Followers. If you have a larger
                order or would like to discuss a customized plan to suit your
                needs, don&apos;t hesitate to</Trans>{' '}
              <TranslatedLink href="/contact-us/"><a><Trans>contact us</Trans></a></TranslatedLink>!
            </p>
          </BoxRow.Col>
        </BoxRow>
      </Animate>
      <Animate>
        <BoxRow className="js-animate-opacity">
          <BoxRow.Col style={{ flexBasis: '56.2%' }}>
            <h4>
              <Trans>Enter details</Trans> <i className="icon-details"></i>
            </h4>
            <p>
              <Trans>
                We never require any sensitive information such as passwords or
                other secure info in order to deliver your Instagram Followers
                or Likes. Simply provide us with your Instagram username and
                your email we&apos;ll get started with your order right away.
              </Trans>
            </p>
          </BoxRow.Col>
          <BoxRow.Col style={{ flexBasis: '42.2%' }}>
            <AnimateDetails className="animate-details" />
          </BoxRow.Col>
        </BoxRow>
      </Animate>
      <Animate>
        <BoxRow className="js-animate-opacity box-row-magic">
          <BoxRow.Col style={{ flexBasis: '43%' }}>
            <AnimateMagic className="animate-magic" />
          </BoxRow.Col>
          <BoxRow.Col style={{ flexBasis: '53.2%' }}>
            <div className="arrow-down">
              <ArrowDown2 />
            </div>
            <h4>
              <Trans>See the magic</Trans> <i className="icon-magic"></i>
            </h4>
            <p>
              <Trans>
                After selecting your package and providing account details (your
                username only), you&apos;re already done! Our process is
                expedited so that it is both fast and easy to provide you with
                the Likes and Followers you need.
              </Trans>
            </p>
          </BoxRow.Col>
        </BoxRow>
      </Animate>
      <div className="divider"></div>
      <Animate>
        <Heading className="ta-c work">
          <h2>
            <Trans>How it Works</Trans>
          </h2>
          <p>
            <Trans>
              Most individuals are totally unwilling to invest time into a
              profile that has little interaction. However, if they are see with
              other members in their social network actively participating, it
              makes investing time into a social media profile both fun and
              easy. Buzzoid is here to help!
            </Trans>
          </p>
        </Heading>
      </Animate>
      <Animate>
        <Grid grid="3">
          <BoxAnimate>
            <BoxAnimate.Image>
              <AnimateDelivery className="animate-delivery" />
            </BoxAnimate.Image>
            <BoxAnimate.Content>
              <h4>
                <Trans>Fastest delivery</Trans>
              </h4>
              <p>
                <Trans>
                  We provide you with the fastest Instagram Followers and Likes
                  in the market. At Buzzoid, you will receive all of your Likes
                  and Followers within an hour after completing your order.
                </Trans>
              </p>
            </BoxAnimate.Content>
          </BoxAnimate>
          <BoxAnimate>
            <BoxAnimate.Image>
              <AnimateGurantee className="animate-gurantee" />
            </BoxAnimate.Image>
            <BoxAnimate.Content>
              <h4>
                <Trans>Our guarantee</Trans>
              </h4>
              <p>
                <Trans>
                  We want to leave a lasting impression on our clients. If you
                  aren&apos;t satisfied with the quality or delivery of your
                  order, tell us. We&apos;ll refund any order that isn&apos;t
                  fulfilled.
                </Trans>
              </p>
            </BoxAnimate.Content>
          </BoxAnimate>
          <BoxAnimate>
            <BoxAnimate.Image>
              <AnimateSupport className="animate-support" />
            </BoxAnimate.Image>
            <BoxAnimate.Content>
              <h4>
                <Trans>24-hour support</Trans>
              </h4>
              <p>
                <Trans>
                  Our dedicated support staff is always available. If you have
                  any questions about our services or experience any problems
                  with your order, please don&apos;t hesitate to contact us.
                </Trans>
              </p>
              {isMobile && (
                <Button className="btn--white" href="/contact-us/">
                  Get in touch
                </Button>
              )}
            </BoxAnimate.Content>
          </BoxAnimate>
        </Grid>
      </Animate>
      <CustomerStories animateStories={false} />
      <Animate>
        <Reviews data={homeReviews} />
      </Animate>
      <Animate>
        <Offers>
          <FancyButtonGroup>
            <FancyButton
              href="/buy-instagram-followers/"
              price="$2.97"
              isMostPopular={true}
              heading={<Trans>Buy Followers</Trans>}
            />
            <FancyButton
              href="/buy-instagram-likes/"
              price="$1.47"
              heading={<Trans>Buy Likes</Trans>}
            />
            <FancyButton
              href="/buy-instagram-views/"
              price="$1.99"
              heading={<Trans>Buy Views</Trans>}
            />
          </FancyButtonGroup>
          <CustomerRated />
        </Offers>
      </Animate>
    </>
  );
};

export default Home;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};
