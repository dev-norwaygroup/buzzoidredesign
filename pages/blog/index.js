/**
 * External dependecies
 */
import { useState } from 'react';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../../utils/utils';
import usePostsIndex from 'api/posts/use-posts-index';
import Spinner from '@/components/spinner/spinner';
import BlogTemplate from '@/components/blog-template/blog-template';

const Blog = () => {
  const [currentPage, setCurrentPage] = useState(1);
  let postsPerPage = 9;

  if (typeof window !== 'undefined') {
    if (window.matchMedia('(max-width: 1023px)').matches) {
      postsPerPage = 4;
    }
  }

  const response = usePostsIndex({
    postsPerPage,
    page: currentPage,
  });

  const posts = response.data?.data;
  const avaiablePages = response.data?.headers['x-wp-totalpages'];

  if (!posts) {
    return <Spinner />;
  }

  return (
    <>
      <BlogTemplate
        routerActive={false}
        avaiablePages={avaiablePages}
        posts={posts}
        setCurrentPage={setCurrentPage}
        currentPage={currentPage}
      />
    </>
  );
};

export default Blog;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};
