/**
 * External dependencies
 */
import { useRef, useEffect } from 'react';
import { useRouter } from 'next/router';
import { i18n } from '@lingui/core';
import { I18nProvider } from '@lingui/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import AOS from 'aos';

/**
 * Internal dependencies
 */
import { initTranslation } from '../utils/utils';
import Layout from '../layout/layout';
import '../assets/scss/main.scss';
import 'aos/dist/aos.css';
import { isLocalePathMatching, getTranslatedPath } from '../utils/utils';
import siteMap from '../statics/site-map';

//initialization function
initTranslation(i18n);

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const locale = router.locale || router.defaultLocale;
  const firstRender = useRef(true);

  useEffect(() => {
    const path = router.asPath;
    if (path == '/') {
      return;
    }

    const isCorrectPath = isLocalePathMatching(path, locale);

    if (! isCorrectPath) {
      const translatedPath = getTranslatedPath(path);

      if (translatedPath) {
        router.push(siteMap[locale][translatedPath], undefined, { shallow: true })
      }
    }
  }, []);

  useEffect(() => {
    setTimeout(() => {
      AOS.init({
        easing: 'ease-out-cubic',
        once: true,
        offset: 50,
      });
    }, 500);
  }, []);

  // run only once on the first render (for server side)
  if (pageProps.translation && firstRender.current) {
    i18n.load(locale, pageProps.translation);
    i18n.activate(locale);
    firstRender.current = false;
  }

  // listen for the locale changes
  useEffect(() => {
    if (pageProps.translation) {
      i18n.load(locale, pageProps.translation);
      i18n.activate(locale);
    }
  }, [locale, pageProps.translation]);

  return (
    <I18nProvider i18n={i18n}>
      <QueryClientProvider client={queryClient}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </QueryClientProvider>
    </I18nProvider>
  );
}

export default MyApp;
