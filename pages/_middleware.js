/**
 * External dependencies
 */
import { NextResponse } from 'next/server'

/**
 * Internal dependencies
 */
import { getTranslatedPath } from 'utils/utils';

export function middleware(request) {
    const locale = request.nextUrl.locale;
    const path = request.nextUrl.pathname;

    if (locale == 'en') {
        return NextResponse.next();
    }

    const translatedPath = getTranslatedPath(path);

    if (translatedPath) {
        const url = request.nextUrl.clone();
        url.pathname = translatedPath;
        return NextResponse.rewrite(url);
    } else {
        return NextResponse.next();
    }
}