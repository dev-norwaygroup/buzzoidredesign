import Head from 'next/head';
import { Trans } from '@lingui/macro';
import * as yup from 'yup';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Animate from '@/components/animate/animate';
import Logos from '@/components/logos/logos';
import Heading from '@/components/heading/heading';
import Reviews from '@/components/reviews/reviews';
import { buyViewsItems } from 'statics/faq';
import { ReviewsFollowersSchema } from 'statics/reviews';
import Offers from '@/components/offers/offers';
import OffersViewsData from '@/blocks/buy-form/data/data-views';
import { viewsIcons } from 'statics/icons';
import { viewsReviews } from 'statics/reviews-temp';
import BuyForm from '@/blocks/buy-form/buy-form';
import { viewsSchema } from 'statics/faq-schemas';

import LogoEntrepremeur from '@/svg/logo-entrepreneur.svg';
import LogoHuffpost from '@/svg/logo-huffpost.svg';
import LogoSej from '@/svg/logo-sej.svg';
import LogoVice from '@/svg/logo-vice.svg';
import LogoVox from '@/svg/logo-vox.svg';
import Grid from '@/components/grid/grid';
import BoxAnimate from '@/components/box-animate/box-animate';
import Faqs from '@/components/faqs/faqs';
import FlexRow from '@/components/flex-row/flex-row';
import ViewsData from '@/blocks/buy-form/data/data-views';
import CustomSVG from '@/components/custom-svg/custom-svg';
import CustomerStories from '@/components/customer-stories/customer-stories';

import AnimateDelivery from '@/svg/animate-delivery.svg';
import AnimateSupport from '@/svg/animate-support.svg';
import AnimateReal from '@/svg/animate-real.svg';

import IconLock from '@/svg/icon-lock.svg';
import IconLighting from '@/svg/icon-lighting.svg';
import IconPlay from '@/svg/icon-play.svg';
import IconUsers from '@/svg/icon-users.svg';
import IconSupport from '@/svg/icon-support.svg';

const schema = yup.object().shape({
  views: yup.string().nullable().required('Please choose a number of likes'),
});

const BuyViewsAlt = () => (
  <>
    <Head>
      <meta name="twitter:image" content="/buy-instagram-views.png" />
      <meta property="og:image:secure_url" content="/buy-instagram-views.png" />
      <meta property="og:image" content="/buy-instagram-views.png" />
      <link rel="alternate" hrefLang="en" href="https://buzzoid.com/buy-instagram-views-alt/" />
      <link rel="alternate" hrefLang="fr" href="https://buzzoid.com/fr/acheter-instagram-views-alt/" />
      <link rel="alternate" hrefLang="de" href="https://buzzoid.com/de/instagram-views-kaufen-alt/" />
      <link rel="alternate" hrefLang="pt" href="https://buzzoid.com/pt/comprar-views-instagram-alt/" />
      <link rel="alternate" hrefLang="nl" href="https://buzzoid.com/nl/instagram-views-kopen-alt/" />
    </Head>
    <div className="intro">
      <div className="shell">
        <div className="intro__inner">
          <div className="intro__entry smaller">
            <h1>
              <Trans>Buy Instagram Views with Instant Delivery</Trans>
            </h1>
            <p className="fullwidth">
              <Trans>
                At Buzzoid, you can buy Instagram views to your videos quickly, safely and easily with just a few clicks. See our deals below!
              </Trans>
            </p>
            <BuyForm fields={ViewsData} schema={schema} />
            <FlexRow grid="2" className="form-icons-grid">
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconPlay width="15" height="17" />
                </div>
                <Trans>Order start in 60 seconds</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconLighting />
                </div>
                <Trans>Fast delivery</Trans> (<Trans>gradual or instant</Trans>)
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconUsers />
                </div>
                <Trans>High quality views</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconSupport />
                </div>
                24/7 <Trans>Live Support</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconLock />
                </div>
                <Trans>No password required</Trans>
              </FlexRow.Col>
            </FlexRow>
          </div>
          <div className="intro__img">
            <CustomSVG type="views" />
          </div>
        </div>
      </div>
    </div>
    <Animate>
      <Logos>
        <LogoEntrepremeur />
        <LogoHuffpost />
        <LogoSej />
        <LogoVice />
        <LogoVox />
      </Logos>
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Ready to Buy Instagram Views?</Trans>
        </h2>
        <p>
          <Trans>
            Instagram video views are an essential part of your marketing
            strategy. Buy Instagram views from Buzzoid, and watch your video
            gain more popularity and brand awareness.
          </Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <Grid grid="3" isUl={true}>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateDelivery className="animate-delivery" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Instant Delivery Guaranteed</Trans>
            </h4>
            <p>
              <Trans>
                Don&apos;t wait to get your views. We send instant views from
                real people.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateReal className="animate-real" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Real Instagram Views</Trans>
            </h4>
            <p>
              <Trans>
                Buying Instagram views doesn&apos;t have to be hard! We only
                deliver 100% organic views.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateSupport className="animate-support" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>24/7 Customer Support</Trans>
            </h4>
            <p>
              <Trans>
                Nothing worse than dealing with bad customer support. We got
                your back.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
      </Grid>
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Buy Instagram Views easily with Buzzoid</Trans>
        </h2>
        <p className="fullwidth">
          <Trans>Over 10,000 daily customers trust us as the best site to deliver real Instagram views</Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <div className="shell">
        <Faqs items={buyViewsItems} />
      </div>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(viewsSchema),
        }}
      ></script>
    </Animate>
    <div className="divider divider--mb-90"></div>
    <CustomerStories />
    <Animate>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(ReviewsFollowersSchema),
        }}
      ></script>
      <Reviews data={viewsReviews} />
    </Animate>
    <Animate>
      <Offers icons={viewsIcons}>
        <BuyForm fields={OffersViewsData} schema={schema} prefix="offers" />
      </Offers>
    </Animate>
  </>
);

export default BuyViewsAlt;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};
