/**
 * External Dependencies
 */
import Head from 'next/head';
import { Trans } from '@lingui/macro';
import Image from 'next/image';
import * as yup from 'yup';
/**
 * External dependencies
 */
import { useRef } from 'react';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Logos from '@/components/logos/logos';
import Animate from '@/components/animate/animate';
import Heading from '@/components/heading/heading';
import BoxRow from '@/components/box-row/box-row';
import Grid from '@/components/grid/grid';
import Faqs from '@/components/faqs/faqs';
import Button from '@/components/button/button';
import FlexRow from '@/components/flex-row/flex-row';
import AutomaticData from '@/blocks/buy-form/data/data-automatic';
import BuyForm from '@/blocks/buy-form/buy-form';
import CustomerStories from '@/components/customer-stories/customer-stories';
import Reviews from '@/components/reviews/reviews';
import Offers from '@/components/offers/offers';
import { autoLikesReviews } from 'statics/reviews-temp';
import { automaticIcons } from 'statics/icons';
import { autoLikesItems } from 'statics/faq';

import AnimateUpload from '@/svg/animate-upload.svg';
import AnimateMedia from '@/svg/animate-media.svg';
import AnimateGrow from '@/svg/animate-grow.svg';
import AnimateHandsReach from '@/svg/animate-hands-reach.svg';
import AnimateDitection from '@/svg/animate-detection.svg';
import AnimateWatch from '@/svg/animate-watch.svg';
import AnimateDynamic from '@/svg/animate-dynamic.svg';
import AnimateDelayLikes from '@/svg/animate-delay-likes.svg';
import AnimateCancel from '@/svg/animate-cancel.svg';
import AnimateCustomSupport from '@/svg/animate-custom-support.svg';
import AnimateMoneyback from '@/svg/animate-moneyback.svg';
import ArrowDown2 from '@/svg/arrow-down2.svg';
import IconSend from '@/svg/icon-send.svg';
import IconCards from '@/svg/icon-cards.svg';
import IconVideo2 from '@/svg/icon-video2.svg';
import IconLock from '@/svg/icon-lock.svg';
import IconLighting from '@/svg/icon-lighting.svg';
import IconChatBubble from '@/svg/icon-chat-bubble.svg';
import IconMoney from '@/svg/icon-money.svg';

import IntroImage from '../assets/images/intro-static2.png';

const autoSchema = {
  "@context": "http://schema.org/",
  "@type": "Product",
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": 5,
    "reviewCount": 23
  },
  "offers": {
    "@type": "Offer",
    "priceCurrency": "USD",
    "price": "9.99",
    "priceValidUntil": "2021-12-30",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "Buzzoid"
    }
  },
  "name": "Buzzoid Auto Likes",
  "description": "A reliable way to boost your likes and help you grow your instagram."
}

const schema = yup.object().shape({
  automaticLikes: yup.string().nullable().required('Please choose a number of likes'),
});


const Automatic = () => {
  const offersRef = useRef(null);
  const scrollToOffers = () => window.scrollTo({ top: offersRef.current.offsetTop - 600, behavior: 'smooth' });

  return (
    <>
      <Head>
        <link rel="alternate" hrefLang="en" href="https://buzzoid.com/automatic-instagram-likes/" />
        <link rel="alternate" hrefLang="fr" href="https://buzzoid.com/fr/automatic-instagram-likes/" />
        <link rel="alternate" hrefLang="de" href="https://buzzoid.com/de/automatic-instagram-likes/" />
        <link rel="alternate" hrefLang="pt" href="https://buzzoid.com/pt/automatic-instagram-likes/" />
        <link rel="alternate" hrefLang="nl" href="https://buzzoid.com/nl/automatic-instagram-likes/" />
      </Head>
      <div className="intro">
        <div className="shell">
          <div className="intro__inner">
            <div className="intro__entry smaller">
              <h1>
                <Trans>Buy Real, Automatic Instagram Likes</Trans>
              </h1>
              <p className="fullwidth">
                <Trans>
                  Why go out of your way to buy Instagram likes manually with
                  every new upload? Our system detects new uploads within 60
                  seconds and sends you real likes from rea l users
                  automatically.
                </Trans>
              </p>
              <BuyForm fields={AutomaticData} schema={schema} />

              <FlexRow grid="2" className="form-icons-grid">
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconCards />
                  </div>
                  <Trans>Up to 4 posts per day</Trans>
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconVideo2 />
                  </div>
                  <Trans>Free views on all videos</Trans>
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconLock />
                  </div>
                  <Trans>Always Safe and Secure</Trans>
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconLighting />
                  </div>
                  <Trans>Real likes from Real Users</Trans>
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconChatBubble />
                  </div>
                  24/7 <Trans>Customer Support</Trans>
                </FlexRow.Col>
                <FlexRow.Col>
                  <div className="form-icons-grid__icon">
                    <IconMoney />
                  </div>
                  <Trans>30-Day Moneyback Guarantee</Trans>
                </FlexRow.Col>
              </FlexRow>
            </div>
            <div className="intro__img">
              <Image
                src={IntroImage}
                width="495"
                height="512"
                alt="intro image"
              />
            </div>
          </div>
        </div>
      </div>
      <Animate>
        <Logos />
      </Animate>
      <Animate>
        <Heading className="ta-c">
          <h2>
            <Trans>How it works</Trans>
          </h2>
        </Heading>
      </Animate>
      <Animate>
        <BoxRow className="align-items-center mb">
          <BoxRow.Col style={{ flexBasis: '36%' }}>
            <AnimateUpload className="animate-upload" />
          </BoxRow.Col>
          <BoxRow.Col style={{ flexBasis: '54%' }}>
            <span className="bullet">1</span>
            <h4>
              <Trans>Upload a picture or video to Instagram</Trans>
            </h4>
            <p>
              <Trans>
                You&apos;re already doing the first step! Just upload a picture
                or video to your Instagram account.
              </Trans>
            </p>
          </BoxRow.Col>
        </BoxRow>
      </Animate>
      <Animate>
        <BoxRow className="align-items-center mb">
          <BoxRow.Col style={{ flexBasis: '54%' }}>
            <span className="bullet">2</span>
            <h4>
              <Trans>Your new media is automatically detected</Trans>
            </h4>
            <p>
              <Trans>
                In just a minute our system will detect your latest upload. You
                don&apos;t have to do anything!
              </Trans>
            </p>
          </BoxRow.Col>
          <BoxRow.Col style={{ flexBasis: '36%' }}>
            <AnimateMedia className="animate-media" />
          </BoxRow.Col>
        </BoxRow>
      </Animate>
      <Animate>
        <BoxRow className="align-items-center box-row--center box-row-likes">
          <BoxRow.Col style={{ flexBasis: '36%' }}>
            <AnimateGrow className="animate-grow" />
          </BoxRow.Col>
          <BoxRow.Col style={{ flexBasis: '54%' }}>
            <div className="arrow-down">
              <ArrowDown2 />
            </div>
            <span className="bullet">3</span>
            <h4>
              <Trans>Watch your likes grow</Trans>
            </h4>
            <p>
              <Trans>
                Instagram auto likes are delivered to your new uploads at your desired speed.
              </Trans>
            </p>
            <button className="btn btn--orange" onClick={scrollToOffers}><Trans>Sign Up</Trans></button>
          </BoxRow.Col>
        </BoxRow>
      </Animate>
      <div className="divider divider--mb-90"></div>
      <Animate>
        <Heading className="ta-c">
          <h2>
            <Trans>Why should you buy automatic Instagram likes?</Trans>
          </h2>
        </Heading>
      </Animate>
      <Animate>
        <Grid grid="2" className="grid--boxes">
          <BoxRow>
            <BoxRow.Col style={{ flexBasis: '40%' }}>
              <AnimateHandsReach className="animate-hands-reach" />
            </BoxRow.Col>
            <BoxRow.Col style={{ flexBasis: '60%' }}>
              <h5 className="h5">
                <Trans>Real users, real likes</Trans>
              </h5>
              <p>
                <Trans>
                  Buzzoid is the first and only service that provides real likes
                  from real users. We only need your Instagram username.
                </Trans>
              </p>
            </BoxRow.Col>
          </BoxRow>
          <BoxRow className="mb-more detection">
            <BoxRow.Col style={{ flexBasis: '40%' }}>
              <AnimateDitection className="animate-detection" />
            </BoxRow.Col>
            <BoxRow.Col style={{ flexBasis: '60%' }}>
              <h5 className="h5">
                <Trans>Automatic detection</Trans>
              </h5>
              <p>
                <Trans>
                  Your new Instagram posts are usually detected within 30
                  seconds of upload.
                </Trans>
              </p>
            </BoxRow.Col>
          </BoxRow>

          <BoxRow>
            <BoxRow.Col style={{ flexBasis: '40%' }}>
              <AnimateWatch className="animate-watch" />
            </BoxRow.Col>
            <BoxRow.Col style={{ flexBasis: '60%' }}>
              <h5 className="h5">
                <Trans>Matching views</Trans>
              </h5>
              <p>
                <Trans>
                  Uploaded a video? We&apos;ll send you the same amount of views
                  as likes, completely free.
                </Trans>
              </p>
            </BoxRow.Col>
          </BoxRow>

          <BoxRow>
            <BoxRow.Col style={{ flexBasis: '40%' }}>
              <AnimateDynamic className="animate-dynamic" />
            </BoxRow.Col>
            <BoxRow.Col style={{ flexBasis: '60%' }}>
              <h5 className="h5">
                <Trans>Dynamic likes</Trans>
              </h5>
              <p>
                <Trans>
                  Not every post will get the same amount of likes. We
                  occasionally send 5-10% more likes to make sure your posts
                  maintain a natural look.
                </Trans>
              </p>
            </BoxRow.Col>
          </BoxRow>

          <BoxRow>
            <BoxRow.Col style={{ flexBasis: '40%' }}>
              <AnimateDelayLikes className="animate-delay-likes" />
            </BoxRow.Col>
            <BoxRow.Col style={{ flexBasis: '60%' }}>
              <h5 className="h5">
                <Trans>Delay your likes</Trans>
              </h5>
              <p>
                <Trans>
                  Likes coming in too fast? Buzzoid lets you adjust the speed.
                </Trans>
              </p>
            </BoxRow.Col>
          </BoxRow>

          <BoxRow>
            <BoxRow.Col style={{ flexBasis: '40%' }}>
              <AnimateCustomSupport className="animate-custom-support" />
            </BoxRow.Col>
            <BoxRow.Col style={{ flexBasis: '60%' }}>
              <h5 className="h5">
                <Trans>Professional customer support</Trans>
              </h5>
              <p>
                <Trans>
                  Our main goal is keeping our customers happy. Contact us any
                  time and expect a helpful response.
                </Trans>
              </p>
            </BoxRow.Col>
          </BoxRow>

          <BoxRow>
            <BoxRow.Col style={{ flexBasis: '40%' }}>
              <AnimateCancel className="animate-cancel" />
            </BoxRow.Col>
            <BoxRow.Col style={{ flexBasis: '60%' }}>
              <h5 className="h5">
                <Trans>Cancel anytime</Trans>
              </h5>
              <p>
                <Trans>
                  Cancel for any reason, any time. No contracts or long term
                  commitments.
                </Trans>
              </p>
            </BoxRow.Col>
          </BoxRow>

          <BoxRow>
            <BoxRow.Col style={{ flexBasis: '40%' }}>
              <AnimateMoneyback className="animate-moneyback" />
            </BoxRow.Col>
            <BoxRow.Col style={{ flexBasis: '60%' }}>
              <h5 className="h5">
                <Trans>Moneyback guarantee</Trans>
              </h5>
              <p>
                <Trans>
                  If for any reason you aren&apos;t satisfied with our service
                  within 30 days, contact us for a full refund, no questions
                  asked.
                </Trans>
              </p>
            </BoxRow.Col>
          </BoxRow>
        </Grid>
        <Animate>
          <div className="get-started">
            <div className="shell">
              <div className="get-started__inner">
                <div className="get-started__entry">
                  <h2><Trans>Ready to get started?</Trans></h2>
                  <span><Trans>It only takes a few minutes to sign up and we will take it from there</Trans>.</span>
                </div>
                <div className="get-started__action">
                  <button className="btn btn--white" onClick={scrollToOffers}><Trans>Get real likes</Trans></button>
                </div>
              </div>
            </div>
          </div>
        </Animate>
      </Animate>
      <div className="divider"></div>
      <CustomerStories
        animateStories={false}
        title={<Trans>What people say about automatic likes</Trans>}
      />
      <Animate>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(autoSchema),
          }}
        ></script>
        <Reviews data={autoLikesReviews} />
      </Animate>
      <Animate>
        <Heading className="ta-c no-space">
          <h2>
            <Trans>Frequently Asked Questions</Trans>
          </h2>
        </Heading>
      </Animate>
      <Animate>
        <div className="shell">
          <Faqs items={autoLikesItems} />
        </div>
      </Animate>
      <Animate>
        <div className="cta-row">
          <div className="shell">
            <p>
              <Trans>To receive more information about Buzzoid service:</Trans>
            </p>
            <Button
              href="/contact-us"
              className="btn--flex btn--rounded btn--shadow btn--black"
            >
              <Trans>Contact Us</Trans>
              <IconSend />
            </Button>
          </div>
        </div>
      </Animate>
      <div ref={offersRef}>
        <Offers icons={automaticIcons}>
          <BuyForm fields={AutomaticData} schema={schema} prefix="offers" />
        </Offers>
      </div>
    </>
  );
};

export default Automatic;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};
