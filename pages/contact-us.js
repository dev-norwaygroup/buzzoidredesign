/**
 * External dependencies
 */
import Head from 'next/head';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import ContactForm from '../blocks/contact-form/contact-form';
import AnimateContact from '@/svg/animate-contact.svg';
import Animate from '@/components/animate/animate';

const ContactUs = () => (
  <>
    <Head>
      <link rel="alternate" hrefLang="en" href="https://buzzoid.com/contact-us/" />
      <link rel="alternate" hrefLang="fr" href="https://buzzoid.com/fr/contactez-nous/" />
      <link rel="alternate" hrefLang="de" href="https://buzzoid.com/de/kontakt/" />
      <link rel="alternate" hrefLang="pt" href="https://buzzoid.com/pt/contato/" />
      <link rel="alternate" hrefLang="nl" href="https://buzzoid.com/nl/contact-ons/" />
    </Head>
    <Animate>
      <div className="shell">
        <div className="contact-us__inner">
          <div className="contact-us__svg">
            <AnimateContact />
          </div>
          <ContactForm />
        </div>
      </div>
    </Animate>
  </>
);

export default ContactUs;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};
