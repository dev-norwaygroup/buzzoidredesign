/**
 * External dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Button from '@/components/button/button';

const Custom404 = () => {
    return (
        <div className="error-page">
            <div className="shell">
                <div className="error-page__inner">
                    <h1><Trans>Oops, we couldn&apos;t find that page</Trans></h1>
                    <Button href="/" className="btn--orange"><Trans>Back to home</Trans></Button>
                </div>
            </div>
        </div>
    )
};

export default Custom404;

export const getStaticProps = async (ctx) => {
    const translation = await loadTranslation(
        ctx.locale,
        process.env.NODE_ENV === 'production'
    );

    return {
        props: {
            translation,
        },
    };
};