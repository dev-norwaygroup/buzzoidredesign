/**
 * External Dependencies
 */
import Head from 'next/head';
import Image from 'next/image';
import { Trans } from '@lingui/macro';
import * as yup from 'yup';
import Link from 'next/link';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Animate from '@/components/animate/animate';
import Logos from '@/components/logos/logos';
import Heading from '@/components/heading/heading';
import Reviews from '@/components/reviews/reviews';
import { ReviewsFollowersSchema } from 'statics/reviews';
import { buyFollowersItems } from 'statics/faq';
import BuyForm from '@/blocks/buy-form/buy-form';
import Offers from '@/components/offers/offers';
import { followersIcons } from 'statics/icons';
import FollowersData from '@/blocks/buy-form/data/data-followers';
import { followersReviews } from 'statics/reviews-temp';
import { followersSchema } from 'statics/faq-schemas';

import Grid from '@/components/grid/grid';
import BoxAnimate from '@/components/box-animate/box-animate';
import Faqs from '@/components/faqs/faqs';
import BuyFormAlt from '@/blocks/buy-form-alt/buy-form-alt';
import FollowersAltData from '@/blocks/buy-form-alt/data/data-followers-alt';
import CustomerStories from '@/components/customer-stories/customer-stories';

import AnimateDelivery from '@/svg/animate-delivery.svg';
import AnimateGurantee from '@/svg/animate-guarantee.svg';
import IconChevronRight from '@/svg/icon-chevron-right-gray.svg';
import AnimateSupport from '@/svg/animate-support.svg';
import CustomerRated from '@/components/customer-rated/customer-rated';
import FlexRow from '@/components/flex-row/flex-row';

import IntroImage from '../assets/images/intro-static5.png';

const schema = yup.object().shape({
  type: yup.string().required(),
  followers: yup.string().when('type', {
    is: 'High Quality Followers',
    then: yup
      .string()
      .nullable()
  }),
  followers_active: yup.string().when('type', {
    is: 'Active Followers',
    then: yup
      .string()
      .nullable()
  }),
});

const BuyFollowersAlt = () => (
  <>
    <Head>
      <meta name="twitter:image" content="/buy-instagram-followers.png" />
      <meta property="og:image:secure_url" content="/buy-instagram-followers.png" />
      <meta property="og:image" content="/buy-instagram-followers.png" />
      <link rel="alternate" hrefLang="en" href="https://buzzoid.com/buy-instagram-followers/" />
      <link rel="alternate" hrefLang="fr" href="https://buzzoid.com/fr/acheter-instagram-followers/" />
      <link rel="alternate" hrefLang="de" href="https://buzzoid.com/de/instagram-followers-kaufen/" />
      <link rel="alternate" hrefLang="pt" href="https://buzzoid.com/pt/comprar-seguidores-instagram/" />
      <link rel="alternate" hrefLang="nl" href="https://buzzoid.com/nl/instagram-followers-kopen/" />
    </Head>
    <div className="intro">
      <div className="shell">
        <div className="intro__inner">
          <div className="intro__entry smaller">
            <div className="sponsored">
              <FlexRow>
                <FlexRow.Col>
                  <div className="sponsored__text">
                    <Trans>SPONSORED</Trans>
                  </div>
                </FlexRow.Col>
                <FlexRow.Col>
                  <Link href="https://growthoid.com/?utm_source=Buzzoid&utm_medium=Followers" passHref>
                    <a target="_blank">
                      <Trans>Managed Growth</Trans> <IconChevronRight />
                    </a>
                  </Link>
                </FlexRow.Col>
              </FlexRow>
            </div>
            <h1>
              <Trans>Buy Instagram Followers with Instant Delivery</Trans>
            </h1>
            <p className="fullwidth">
              <Trans>
                At Buzzoid, you can buy Instagram followers quickly, safely
                and easily with just a few clicks. See our deals below!
              </Trans>
            </p>
            <CustomerRated />

            <BuyFormAlt fields={FollowersAltData} schema={schema} />
          </div>
          <div className="intro__img">
            <Image
              src={IntroImage}
              width={557}
              height={580}
              alt="intro image"
            />
          </div>
        </div>
      </div>
    </div>
    <Animate>
      <Logos />
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Ready to Buy Instagram Followers?</Trans>
        </h2>
        <p>
          <Trans>
            Followers are a fundamental part of your Instagram success. Buy Instagram followers from Buzzoid, and watch your Instagram profile gain more recognition, visibility and exposure.
          </Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <Grid grid="3" isUl={true}>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateDelivery className="animate-delivery" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Instant Delivery Guaranteed</Trans>
            </h4>
            <p>
              <Trans>
                Don&apos;t wait to get your followers. Orders typically process within minutes of purchase.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateGurantee className="animate-gurantee" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>High Quality Followers</Trans>
            </h4>
            <p>
              <Trans>
                We offer the best quality followers the market has to offer. No fake Instagram followers!
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateSupport className="animate-support" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>24/7 Customer Support</Trans>
            </h4>
            <p>
              <Trans>
                Nothing worse than dealing with bad customer support. We got your back.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
      </Grid>
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Buy Instagram Followers Easily With Buzzoid</Trans>
        </h2>
        <p className="fullwidth">
          <Trans>Over 10,000 daily customers trust us as the best site to deliver real Instagram followers</Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <div className="shell">
        <Faqs items={buyFollowersItems} />
      </div>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(followersSchema),
        }}
      ></script>
    </Animate>
    <div className="divider divider--mb-90"></div>
    <CustomerStories />
    <Animate>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(ReviewsFollowersSchema),
        }}
      ></script>
      <Reviews data={followersReviews} />
    </Animate>
    <Animate>
      <Offers icons={followersIcons}>
        <BuyForm fields={FollowersData} schema={schema} prefix="offers" />
      </Offers>
    </Animate>
  </>
);

export default BuyFollowersAlt;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};
