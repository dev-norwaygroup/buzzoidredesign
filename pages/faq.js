/**
 * External dependencies
 */
import { Trans } from '@lingui/macro';
import Head from 'next/head';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import FaqIntro from '@/components/faq-intro/faq-intro';
import Offers from '@/components/offers/offers';
import FancyButtonGroup from '@/components/button/fancy-button-group';
import CustomerRated from '@/components/customer-rated/customer-rated';
import Animate from '@/components/animate/animate';
import FancyButton from '@/components/button/fancy-button';
import { faqSchema } from 'statics/faq-schemas';

const Faq = () => {
  return (
    <>
      <Head>
        <link rel="alternate" hrefLang="en" href="https://buzzoid.com/faq/" />
        <link rel="alternate" hrefLang="fr" href="https://buzzoid.com/fr/faq/" />
        <link rel="alternate" hrefLang="de" href="https://buzzoid.com/de/faq/" />
        <link rel="alternate" hrefLang="pt" href="https://buzzoid.com/pt/faq/" />
        <link rel="alternate" hrefLang="nl" href="https://buzzoid.com/nl/faq/" />
      </Head>
      <Animate>
        <FaqIntro />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(faqSchema),
          }}
        ></script>
      </Animate>
      <Animate>
        <Offers>
          <FancyButtonGroup>
            <FancyButton
              href="/buy-instagram-followers/"
              price="$2.97"
              isMostPopular={true}
              heading={<Trans>Buy Followers</Trans>}
            />
            <FancyButton
              href="/buy-instagram-likes/"
              price="$1.47"
              heading={<Trans>Buy Likes</Trans>}
            />
            <FancyButton
              href="/buy-instagram-views/"
              price="$1.99"
              heading={<Trans>Buy Views</Trans>}
            />
          </FancyButtonGroup>
          <CustomerRated />
        </Offers>
      </Animate>
    </>
  );
};

export default Faq;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};


