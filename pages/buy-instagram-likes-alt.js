import Head from 'next/head';
import * as yup from 'yup';
import { Trans } from '@lingui/macro';
import Image from 'next/image';

/**
 * Internal dependencies
 */
import { loadTranslation } from '../utils/utils';
import Animate from '@/components/animate/animate';
import Logos from '@/components/logos/logos';
import Heading from '@/components/heading/heading';
import { buyLikesItems } from '../statics/faq';
import { ReviewsInstagramSchema } from '../statics/reviews';
import { likesIcons } from 'statics/icons';
import { likesReviews } from 'statics/reviews-temp';
import { likesSchema } from 'statics/faq-schemas';

import Grid from '@/components/grid/grid';
import BoxAnimate from '@/components/box-animate/box-animate';
import Faqs from '@/components/faqs/faqs';
import Reviews from '@/components/reviews/reviews';
import FlexRow from '@/components/flex-row/flex-row';
import BuyForm from '@/blocks/buy-form/buy-form';
import LikesData from '@/blocks/buy-form/data/data-likes';
import CustomerStories from '@/components/customer-stories/customer-stories';
import Offers from '@/components/offers/offers';

import AnimateDelivery from '@/svg/animate-delivery.svg';
import AnimateGurantee from '@/svg/animate-guarantee.svg';
import AnimateSupport from '@/svg/animate-support.svg';

import IconVideo2 from '@/svg/icon-video2.svg';
import IconLock from '@/svg/icon-lock.svg';
import IconLighting from '@/svg/icon-lighting.svg';
import IconShield from '@/svg/icon-shield.svg';
import IconUsers from '@/svg/icon-users.svg';
import IconSplit from '@/svg/icon-split.svg';
import IconSupport from '@/svg/icon-support.svg';
import IntroImage from '../assets/images/intro-static3.png';

const schema = yup.object().shape({
  type: yup.string().required(),
  likes: yup.string().when('type', {
    is: 'High Quality Likes',
    then: yup.string().nullable().required('Please choose a number of likes'),
  }),
  likes_premium: yup.string().when('type', {
    is: 'Premium Likes',
    then: yup.string().nullable().required('Please choose a number of likes'),
  }),
});

const BuyLikes = () => (
  <>
    <Head>
      <meta name="twitter:image" content="/buy-instagram-likes.png" />
      <meta property="og:image:secure_url" content="/buy-instagram-likes.png" />
      <meta property="og:image" content="/buy-instagram-likes.png" />
      <link rel="alternate" hrefLang="en" href="https://buzzoid.com/buy-instagram-likes-alt/" />
      <link rel="alternate" hrefLang="fr" href="https://buzzoid.com/fr/acheter-instagram-likes-alt/" />
      <link rel="alternate" hrefLang="de" href="https://buzzoid.com/de/instagram-likes-kaufen-alt/" />
      <link rel="alternate" hrefLang="pt" href="https://buzzoid.com/pt/comprar-curtidas-instagram-alt/" />
      <link rel="alternate" hrefLang="nl" href="https://buzzoid.com/nl/instagram-likes-kopen-alt/" />
    </Head>
    <div className="intro">
      <div className="shell">
        <div className="intro__inner">
          <div className="intro__entry smaller">
            <h1>
              <Trans>Buy Instagram Likes with Instant Delivery</Trans>
            </h1>
            <p className="fullwidth">
              <Trans>
                At Buzzoid, you can buy Instagram likes quickly, safely and
                easily with just a few clicks. See our deals below!
              </Trans>
            </p>
            <BuyForm fields={LikesData} schema={schema} />
            <FlexRow grid="2" className="form-icons-grid">
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconShield width="18" height="18" />
                </div>
                <Trans>Guaranteed Instant Delivery</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconUsers />
                </div>
                <Trans>REAL likes from REAL people</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconSplit />
                </div>
                <Trans>Option to split likes on multiple pictures</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconLock />
                </div>
                <Trans>No password required</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconVideo2 />
                </div>
                <Trans>Includes video views</Trans>
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconLighting />
                </div>
                <Trans>Fast delivery</Trans> (<Trans>gradual or instant</Trans>)
              </FlexRow.Col>
              <FlexRow.Col>
                <div className="form-icons-grid__icon">
                  <IconSupport />
                </div>
                24/7 <Trans>Live Support</Trans>
              </FlexRow.Col>
            </FlexRow>
          </div>
          <div className="intro__img">
            <Image
              src={IntroImage}
              width="437"
              height="423"
              alt="intro image"
            />
          </div>
        </div>
      </div>
    </div>
    <Animate>
      <Logos />
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Ready to Buy Instagram Likes?</Trans>
        </h2>
        <p>
          <Trans>
            Buying likes for your Instagram posts is the best way to gain more
            engagement and success. Improve your social media marketing strategy
            with Buzzoid.
          </Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <Grid grid="3" isUl={true}>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateDelivery className="animate-delivery" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>Instant Delivery Guaranteed</Trans>
            </h4>
            <p>
              <Trans>
                Don&apos;t wait to get your likes. Orders typically process within minutes of purchase.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateGurantee className="animate-gurantee" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>100% Real Likes</Trans>
            </h4>
            <p>
              <Trans>
                Get high quality, instant likes from real users with real accounts
              </Trans>
              <span className="block">
                (<Trans>no fake accounts</Trans>).
              </span>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
        <BoxAnimate isLi={true}>
          <BoxAnimate.Image>
            <AnimateSupport className="animate-support" />
          </BoxAnimate.Image>
          <BoxAnimate.Content>
            <h4>
              <Trans>24/7 Customer Support</Trans>
            </h4>
            <p>
              <Trans>
                Nothing worse than dealing with bad customer support. We strive to provide quality service.
              </Trans>
            </p>
          </BoxAnimate.Content>
        </BoxAnimate>
      </Grid>
    </Animate>
    <Animate>
      <Heading className="ta-c">
        <h2>
          <Trans>Buy Instagram Likes easily with Buzzoid</Trans>
        </h2>
        <p className="fullwidth">
        <Trans>Over 10,000 daily customers trust us as the best site to deliver real Instagram likes</Trans>
        </p>
      </Heading>
    </Animate>
    <Animate>
      <div className="shell">
        <Faqs items={buyLikesItems} />
      </div>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(likesSchema),
        }}
      ></script>
    </Animate>
    <div className="divider divider--mb-90"></div>
    <CustomerStories />
    <Animate>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(ReviewsInstagramSchema),
        }}
      ></script>
      <Reviews data={likesReviews} />
      <Animate>
        <Offers icons={likesIcons}>
          <BuyForm fields={LikesData} schema={schema} prefix="offers" />
        </Offers>
      </Animate>
    </Animate>
  </>
);

export default BuyLikes;

export const getStaticProps = async (ctx) => {
  const translation = await loadTranslation(
    ctx.locale,
    process.env.NODE_ENV === 'production'
  );

  return {
    props: {
      translation,
    },
  };
};
