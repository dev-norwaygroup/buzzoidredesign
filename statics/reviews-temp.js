/**
 * Internal dependencies
 */
import Account2Img from '../assets/images/account-2.png';
import Account3Img from '../assets/images/account-3.png';
import Account4Img from '../assets/images/account-4.png';

const homeReviews = [
    {
        text: "Instagram really helps to build our presence in social media. We were blow away by Buzzoid's speed and quality for likes and followers!",
        author: 'Chris Johnson'
    },
    {
        text: "Didn't get the followers instantly but The customer support is so fast! Totally worth",
        author: 'James'
    },
    {
        text: 'I swear this has to be the best website I have ever come across. I needed specifically just views for a video my friend made, and oh boy did they deliver. Thanks guys!',
        author: 'Jens'
    },
    {
        text: 'Visiting for the first time. Quick likes, speedy and worth-the-cash services is what I received.',
        author: 'Michael Manley'
    },
    {
        text: 'Made payment at night, woke up with three times the followers! Is it a dream? No, just Buzzoid! Hahaha, thanks again gentlemen. Top service you guys have here',
        author: 'Jack'
    },
    {
        text: 'Buzzoid has me spellbound. It provides such radical results at extremely cheap rates are unbelievable.',
        author: 'Cecil'
    },
    {
        text: 'Amazing works perfect and on time',
        author: 'ultimate_human_freedom'
    },
];

const likesReviews = [
    {
        text: "How did you make verified users with hundreds of thousands of followers to like my picture for such a cheap price? You should increase the prices on premium likes, seriously. I would be willing to pay way more for this service and keep the cheapskates out.",
        author: 'Vlad P'
    },
    {
        text: 'They are good and real',
        author: 'sagebonque'
    },
    {
        text: 'Actually worked and very professional',
        author: 'Harry'
    },
    {
        text: 'Awesome app and highly recommend',
        author: 'Jose Fernandez'
    },
    {
        text: 'Highly recommend this platform.',
        author: 'Anandhu'
    },
    {
        text: 'It works so well thank you so much !! Will order again !',
        author: 'Dan'
    },
    {
        text: 'Very good app and trusted',
        author: 'Akshat'
    },
    {
        text: 'Best app ever...it really gives you real likes',
        author: 'Vaibhav Donerao'
    },
    {
        text: 'Received 100% authentic likes within a blink of an eye. See it for yourself.',
        author: 'Sujan'
    },
    {
        text: 'Bought affordable ig likes from Buzzoid and I am not disappointed. Try it out!',
        author: 'Diar'
    },
    {
        text: 'I decided to buy instagram likes and the request was processed in seconds. Satisfied!',
        author: 'Moshan'
    },
    {
        text: 'Ordered for Instagram likes in Buzzoid and received them in minutes. Incredibly fast services.',
        author: 'Patel'
    },
    {
        text: 'The quality likes from the house of Buzzoid has kept me spellbound. Thanks!',
        author: 'Aniston'
    },
    {
        text: 'The quick delivery was not expected. But, to my surprise Buzz has come up with an extraordinary service.',
        author: 'Habib'
    },
    {
        text: 'I have bought instagram likes from Buzzoid.com and their quick delivery has made the experience really great!',
        author: 'Harry'
    },
    {
        text: 'Quick delivery of 1000 likes and easy payment has really left me mesmerized. Keep it up guys!',
        author: 'Jack'
    },
    {
        text: "Hey Guys! It’s pretty amazing the way you offer the service. These likes works wonders on my business!",
        author: 'Joshua'
    },
    {
        text: 'Was not sure whether to use the service, but invested $40 to buy REAL likes and seriously I got complete value of money. Thanks guys!',
        author: 'Henry'
    },
    {
        text: 'With lots of doubt in mind, I have tried the Buzzoid service and now it has become simply irresistible. Love you guys!',
        author: 'Ashton Freeman'
    },
    {
        text: 'The jaw-dropping service from these guys has really helped my business to grow. I bought 500 likes which turned into 1500 (I think I hit the explore page) within few days.',
        author: 'Ashley'
    }
];

const followersReviews = [
    {
        text: 'Love this app!',
        author: 'Ellie'
    },
    {
        text: 'better than all other websites and this one actually works!',
        author: 'andre'
    },
    {
        text: "Great service and like that you're given a trial before paying",
        author: 'Kay'
    },
    {
        text: 'Awesome app and highly recommend',
        author: 'Jose Fernandez'
    },
    {
        text: 'It actually works and all of them were legit !!',
        author: 'Jason'
    },
    {
        text: 'Really quick and easy',
        author: 'Bailey'
    },
    {
        text: 'Amazing, so helpful and has helped me improve my page and earn more followers with the positive engagement increase.',
        author: 'Matt Cromer'
    },
    {
        text: 'Great way to get followers',
        author: 'Kamerin carter'
    },
    {
        text: 'It worked',
        author: 'Alex'
    },
    {
        text: '8 Ways to Increase Your Instagram Followers Without Following Back',
        author: 'Aadil'
    },
    {
        text: 'I am addicted, it is happening instantly but my only question is it possible to choose your audience? eg country ...',
        author: 'Esther'
    },
    {
        text: 'Paid $10 to buy followers. Took several minutes to work, but this is legit!',
        author: 'Kirk'
    },
    {
        text: 'Definitely a great service! Thank you!',
        author: 'C'
    },
    {
        text: 'I have used their offers and to buy Instagram followers and i like them',
        author: 'Muhammad Junaid'
    },
    {
        text: 'We went with the 2500 package and worked within the hour. However, we’ve lost 60 followers so far. Not sure if it’s because IG deleted those accounts or what. So something to lookout for',
        author: 'Jav'
    },
    {
        text: 'Bought there service countless times .. They always delivered more than ordered. Thumbs up!!',
        author: 'Aamir'
    },
    {
        text: 'Worked!',
        author: 'Lisa Lee'
    },
    {
        text: 'I was waaay to skeptical,but I gave it a try .After 5mins I got all my new followers,thanks!!',
        author: 'Lalsa'
    },
    {
        text: 'Nice service in low rate they give good followers :)',
        author: 'Arvind'
    },
    {
        text: 'Was skeptical at first, But they delivered right away. Prompt customer service as well.',
        author: 'Angela'
    }
];

const viewsReviews = [
    {
        text: 'It worked really fast',
        author: 'Jaiden Tate'
    },
    {
        text: 'Thank you!! Very fast',
        author: 'Kiyo'
    },
    {
        text: "Very good thanks",
        author: 'Bahrom'
    },
    {
        text: 'Always worked excellent',
        author: 'Maya'
    },
    {
        text: 'Incredibly fast service',
        author: 'Greg'
    },
    {
        text: 'Absolutely amazing service',
        author: 'Dan'
    },
    {
        text: 'I love this app! Flawless',
        author: 'Roy'
    },
    {
        text: 'Great way to gain real followers',
        author: 'Warriorx_kingup'
    },
    {
        text: 'The best site to buy Instagram Statics.....',
        author: 'Amit Gautam'
    },
    {
        text: 'If you buy likes fromt his website, they give you views as well, but I needed a lot more views than likes so these packages helped me out a lot',
        author: 'Hayden'
    },
    {
        text: 'I swear this has to be the best website I have ever come across. I needed specifically just views for a video my friend made, and oh boy did they deliver. Thanks guys!',
        author: 'Jens'
    },
    {
        text: 'I bought service and deliver asap thanks great experience. Fast delivery',
        author: 'habib ullah'
    },
    {
        text: 'Worked as advertised and very quick delivery within seconds of completing the payment. Keep it up guys!',
        author: 'Josh'
    },
    {
        text: 'It is legit and does actually work... And as keemstar would say: " Fast AF Boiii',
        author: 'Irshaad OOZEER'
    },
    {
        text: 'Quick, Fast & Easy. The pricing is perfect and I was very surprised that I received followers so quickly. The followers I bought, immediately had an impact on my social media presence. I had new purchases from new customers in the same day.',
        author: 'Love'
    },
    {
        text: 'WOW! I am just amazed how fast and cheap those likes are. unlike other providers, these are REAL too!!!',
        author: 'Sindey'
    },
    {
        text: 'Holy crap, this is so FAST! Also, if you guys care about quality, the likes from Buzzoid is actually 100% real people and not bots.',
        author: 'Jens'
    },
    {
        text: 'Fast and reliable and quick with delivery. 100% real Deal',
        author: 'Pele'
    },
    {
        text: 'IT WORKS!',
        author: 'Boi'
    },
    {
        text: 'Five stars from me for their brilliance!',
        author: 'Elizabeth'
    },
];

const autoLikesReviews = [
    {
        text: "Likes are instant and affordable. I’ve used other companies and instagram would block me from liking photos and threaten to delete my account.",
        author: 'Victoria'
    },
    {
        text: 'I get my likes right away',
        author: 'Juan Ramon Jimenez Guillem',
        imgSrc: Account3Img
    },
    {
        text: "They are definitely one of the cheapest likes providers out there. The great thing is the quality they deliver is really good.",
        author: 'Eleanor Robson',
        imgSrc: Account2Img
    },
    {
        text: "Buzzoid might actually be one of the best apps I’ve used for ig, I just wish I had a choice to choose between male or female “likes” lol I think that would be cool to have that as an option but other than that buzzoid “likes” are definitely real, also they have great customer service",
        author: 'Prince Cannon',
        imgSrc: Account4Img
    },
]


export {
    homeReviews,
    likesReviews,
    followersReviews,
    viewsReviews,
    autoLikesReviews
}