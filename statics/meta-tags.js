const metaTagsMap = {
    'home': {
        datePublished: '2014-05-06T01:46:08+00:00',
        dateModified: '2021-11-29T09:09:22+00:00'
    },
    'buy-instagram-likes': {
        datePublished: '2014-05-06T01:46:17+00:00',
        dateModified: '2021-11-29T09:07:19+00:00'
    },
    'buy-instagram-likes-alt': {
        datePublished: '2014-05-06T01:46:17+00:00',
        dateModified: '2021-11-29T09:07:19+00:00'
    },
    'automatic-instagram-likes': {
        datePublished: '2014-05-06T01:46:17+00:00',
        dateModified: '2019-12-12T20:08:55+00:00'
    },
    'buy-instagram-followers': {
        datePublished: '2014-05-06T01:46:26+00:00',
        dateModified: '2021-11-29T09:07:29+00:00'
    },
    'buy-instagram-followers-alt': {
        datePublished: '2014-05-06T01:46:26+00:00',
        dateModified: '2021-11-29T09:07:29+00:00'
    },
    'buy-instagram-views': {
        datePublished: '2016-05-20T05:55:11+00:00',
        dateModified: '2021-01-14T19:39:39+00:00'
    },
    'buy-instagram-views-alt': {
        datePublished: '2016-05-20T05:55:11+00:00',
        dateModified: '2021-01-14T19:39:39+00:00'
    },
    'faq': {
        datePublished: '2014-05-06T01:46:31+00:00',
        dateModified: '2018-12-12T16:19:54+00:00'   
    },
    'contact-us': {
        datePublished: '2014-05-06T01:45:29+00:00',
        dateModified: '2018-12-17T15:22:12+00:00'  
    },
    'blog': {
        datePublished: '2019-10-23T17:19:12+00:00',
        dateModified: '2019-11-04T03:33:00+00:00'  
    },
    'posts': {
        datePublished: '2020-11-26T03:45:10+00:00',
        dateModified: '2020-11-26T04:03:15+00:00'
    },
    'terms-of-service': {
        datePublished: '2019-05-08T16:33:50+00:00',
        dateModified: '2021-05-14T14:51:12+00:00'
    },
    'privacy-policy': {
        datePublished: '2018-05-18T15:14:50+00:00',
        dateModified: '2018-12-17T15:29:14+00:00'
    },
    'en': {
        'home': {
            title: 'Buzzoid - Buy Instagram Followers and Likes starting at $2.97',
            description: 'We got the competition shaking due to our extremely low prices, unbelievably cheap prices, and amazing customer support. Instagram services just got easier.'
        },
        'buy-instagram-likes': {
            title: 'Buy Instagram Likes - 100% Real & Instant Likes | Now $1.47',
            description: 'Buy Instagram likes from Buzzoid for as little as $1.47. Instant delivery, real likes, and friendly 24/7 customer support. Try us today.'
        },
        'buy-instagram-likes-alt': {
            title: 'Buy Instagram Likes - 100% Real & Instant Likes | Now $1.47',
            description: 'Buy Instagram likes from Buzzoid for as little as $1.47. Instant delivery, real likes, and friendly 24/7 customer support. Try us today.'
        },
        'automatic-instagram-likes': {
            title: 'Buy Automatic Instagram Likes - 100% Real &amp; Instant Delivery',
            description: 'We got the competition shaking due to our extremely low prices, unbelievably cheap prices, and amazing customer support. Instagram services just got easier.',
            descriptionOG: 'Buy automatic Instagram likes from Buzzoid with our affordable monthly packages. Instant delivery, real likes and friendly 24/7 customer support. Try us today.'
        },
        'buy-instagram-followers': {
            title: 'Buy Instagram Followers - 100% Real & Instant | Now $2.97',
            description: 'Buy Instagram followers from Buzzoid for as little as $2.97. Instant delivery, real followers, and friendly 24/7 customer support. Try us today.'
        },
        'buy-instagram-followers-alt': {
            title: 'Buy Instagram Followers - 100% Real & Instant | Now $2.97',
            description: 'Buy Instagram followers from Buzzoid for as little as $2.97. Instant delivery, real followers, and friendly 24/7 customer support. Try us today.'
        },
        'buy-instagram-views': {
            title: 'Buy Instagram Views - Real & Instant Delivery | Now $1.99',
            description: 'If you want to buy Instagram views, things are about to get a lot easier. Buzzoid has competitive prices, fast delivery speed, and friendly support.'
        },
        'buy-instagram-views-alt': {
            title: 'Buy Instagram Views - Real & Instant Delivery | Now $1.99',
            description: 'If you want to buy Instagram views, things are about to get a lot easier. Buzzoid has competitive prices, fast delivery speed, and friendly support.'
        },
        'faq': {
            title: 'Frequently Asked Questions - Buzzoid',
            description: 'Questions that you may want to ask that have been asked before. Read through them to make sure you&#039;re on the right page.'
        },
        'contact-us': {
            title: 'Contact Us - Buzzoid',
            description: undefined
        },
        'blog': {
            title: 'Instagram Marketing Blog: Strategies, Tips & Guides | Buzzoid',
            description: 'Get the freshest and latest news about Instagram marketing directly from Buzzoid. We provide you with all the best guides to grow your Instagram account.'
        },
        'posts': {
            title: undefined,
            description: undefined
        },
        'terms-of-service': {
            title: 'Terms of Service - Buzzoid',
            description: 'Terms of use Thanks for using our products and services (“Services”). The Services are provided by Perago LLC, +1 855-848-9812. By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any …'
        },
        'privacy-policy': {
            title: 'Privacy Policy - Buzzoid',
            description: 'Privacy Policy We take your privacy seriously and will take all measures to protect your personal information. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. Any personal information received will only be used to fill your order. We …'
        },
    },
    'fr': {
        'home': {
            title: 'Buzzoid - Acheter Followers Instagram et Likes partir de $2,97',
            description: 'Nous avons fait trembler la concurrence en raison de nos prix extrêmement bas, de nos prix incroyablement bas et de notre service client exceptionnel. Les services Instagram sont devenus plus simples.'
        },
        'buy-instagram-likes': {
            title: 'Achetez des Likes Instagram - 100% Likes réels et instantanés | Maintenant $1.47',
            description: "Achetez des likes Instagram auprès de Buzzoid pour aussi peu que $1.47. Livraison instantanée, vrais likes et support client amical 24h/24 et 7j/7. Essayez-nous aujourd'hui."
        },
        'buy-instagram-likes-alt': {
            title: 'Achetez des Likes Instagram - 100% Likes réels et instantanés | Maintenant $1.47',
            description: "Achetez des likes Instagram auprès de Buzzoid pour aussi peu que $1.47. Livraison instantanée, vrais likes et support client amical 24h/24 et 7j/7. Essayez-nous aujourd'hui."
        },
        'automatic-instagram-likes': {
            title: 'Buy Automatic Instagram Likes - 100% Real &amp; Instant Delivery',
            description: 'We got the competition shaking due to our extremely low prices, unbelievably cheap prices, and amazing customer support. Instagram services just got easier.',
            descriptionOG: 'Buy automatic Instagram likes from Buzzoid with our affordable monthly packages. Instant delivery, real likes and friendly 24/7 customer support. Try us today.'
        },
        'buy-instagram-followers': {
            title: 'Acheter des abonnés Instagram - 100% réel et instantané | Maintenant $2.97',
            description: "Achetez des abonnés Instagram auprès de Buzzoid pour aussi peu que $2.97. Livraison instantanée, vrais abonnés et support client amical 24h/24 et 7j/7. Essayez-nous aujourd'hui."
        },
        'buy-instagram-followers-alt': {
            title: 'Acheter des abonnés Instagram - 100% réel et instantané | Maintenant $2.97',
            description: "Achetez des abonnés Instagram auprès de Buzzoid pour aussi peu que $2.97. Livraison instantanée, vrais abonnés et support client amical 24h/24 et 7j/7. Essayez-nous aujourd'hui."
        },
        'buy-instagram-views': {
            title: 'Achetez des vues Instagram - Livraison réelle et instantanée | Maintenant $1.99',
            description: 'Si vous voulez acheter des vues Instagram, les choses vont devenir beaucoup plus faciles. Buzzoid a des prix compétitifs, une rapidité de livraison et un support convivial.'
        },
        'buy-instagram-views-alt': {
            title: 'Achetez des vues Instagram - Livraison réelle et instantanée | Maintenant $1.99',
            description: 'Si vous voulez acheter des vues Instagram, les choses vont devenir beaucoup plus faciles. Buzzoid a des prix compétitifs, une rapidité de livraison et un support convivial.'
        },
        'faq': {
            title: 'Foire aux questions - Buzzoid',
            description: 'Des questions que vous voudrez peut-être poser et qui ont déjà été posées. Lisez-les pour vous assurer que vous êtes sur la bonne page.'
        },
        'contact-us': {
            title: 'Contactez-nous - Buzzoid',
            description: undefined
        },
        'blog': {
            title: 'Instagram Marketing Blog: Strategies, Tips & Guides | Buzzoid',
            description: 'Get the freshest and latest news about Instagram marketing directly from Buzzoid. We provide you with all the best guides to grow your Instagram account.'
        },
        'posts': {
            title: undefined,
            description: undefined
        },
        'terms-of-service': {
            title: 'Terms of Service - Buzzoid',
            description: 'Terms of use Thanks for using our products and services (“Services”). The Services are provided by Perago LLC, +1 855-848-9812. By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any …'
        },
        'privacy-policy': {
            title: 'Privacy Policy - Buzzoid',
            description: 'Privacy Policy We take your privacy seriously and will take all measures to protect your personal information. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. Any personal information received will only be used to fill your order. We …'
        },
    },
    'de': {
        'home': {
            title: 'Buzzoid - Kaufe Instagram Follower und Likes ab $2.97',
            description: 'Wir haben die Konkurrenz aufgrund unserer extrem niedrigen Preise, unglaublich günstigen Preise und unseres großartigen Kundensupports ins Wanken gebracht. Instagram-Dienste sind jetzt noch einfacher.'
        },
        'buy-instagram-likes': {
            title: 'Kaufen Sie Instagram Likes - 100% Real & Instant Likes | Jetzt $1.47',
            description: 'Kaufen Sie Instagram-Likes von Buzzoid für nur $1.47. Sofortige Lieferung, echte Likes und freundlicher 24/7-Kundensupport. Testen Sie uns noch heute.'
        },
        'buy-instagram-likes-alt': {
            title: 'Kaufen Sie Instagram Likes - 100% Real & Instant Likes | Jetzt $1.47',
            description: 'Kaufen Sie Instagram-Likes von Buzzoid für nur $1.47. Sofortige Lieferung, echte Likes und freundlicher 24/7-Kundensupport. Testen Sie uns noch heute.'
        },
        'automatic-instagram-likes': {
            title: 'Buy Automatic Instagram Likes - 100% Real &amp; Instant Delivery',
            description: 'We got the competition shaking due to our extremely low prices, unbelievably cheap prices, and amazing customer support. Instagram services just got easier.',
            descriptionOG: 'Buy automatic Instagram likes from Buzzoid with our affordable monthly packages. Instant delivery, real likes and friendly 24/7 customer support. Try us today.'
        },
        'buy-instagram-followers': {
            title: 'Kaufen Sie Instagram Follower - 100% Real & Instant | Jetzt $2.97',
            description: 'Kaufen Sie Instagram-Follower von Buzzoid für nur $2.97. Sofortige Lieferung, echte Follower und freundlicher 24/7-Kundensupport. Testen Sie uns noch heute.'
        },
        'buy-instagram-followers-alt': {
            title: 'Kaufen Sie Instagram Follower - 100% Real & Instant | Jetzt $2.97',
            description: 'Kaufen Sie Instagram-Follower von Buzzoid für nur $2.97. Sofortige Lieferung, echte Follower und freundlicher 24/7-Kundensupport. Testen Sie uns noch heute.'
        },
        'buy-instagram-views': {
            title: 'Kaufen Sie Instagram Views - Real & Instant Delivery | Jetzt $1.99',
            description: 'Wenn Sie Instagram-Ansichten kaufen möchten, werden die Dinge viel einfacher. Buzzoid bietet wettbewerbsfähige Preise, schnelle Lieferzeiten und freundlichen Support.'
        },
        'buy-instagram-views-alt': {
            title: 'Kaufen Sie Instagram Views - Real & Instant Delivery | Jetzt $1.99',
            description: 'Wenn Sie Instagram-Ansichten kaufen möchten, werden die Dinge viel einfacher. Buzzoid bietet wettbewerbsfähige Preise, schnelle Lieferzeiten und freundlichen Support.'
        },
        'faq': {
            title: 'Häufig gestellte Fragen - Buzzoid',
            description: 'Fragen, die Sie möglicherweise stellen möchten, wurden bereits gestellt. Lesen Sie sie durch, um sicherzustellen, dass Sie auf der richtigen Seite sind.'
        },
        'contact-us': {
            title: 'Kontaktieren Sie uns - Buzzoid',
            description: undefined
        },
        'blog': {
            title: 'Instagram Marketing Blog: Strategies, Tips & Guides | Buzzoid',
            description: 'Get the freshest and latest news about Instagram marketing directly from Buzzoid. We provide you with all the best guides to grow your Instagram account.'
        },
        'posts': {
            title: undefined,
            description: undefined
        },
        'terms-of-service': {
            title: 'Terms of Service - Buzzoid',
            description: 'Terms of use Thanks for using our products and services (“Services”). The Services are provided by Perago LLC, +1 855-848-9812. By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any …'
        },
        'privacy-policy': {
            title: 'Privacy Policy - Buzzoid',
            description: 'Privacy Policy We take your privacy seriously and will take all measures to protect your personal information. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. Any personal information received will only be used to fill your order. We …'
        },
    },
    'nl': {
        'home': {
            title: 'Buzzoid - Koop Instagram Volgers en Likes vanaf $2,97',
            description: 'We hebben de concurrentie doen schudden vanwege onze extreem lage prijzen, ongelooflijk goedkope prijzen en geweldige klantenondersteuning. Instagram-services zijn nu nog eenvoudiger geworden.'
        },
        'buy-instagram-likes': {
            title: 'Koop Instagram Likes - 100% Real & Instant Likes | Nu $1.47',
            description: 'Koop Instagram-likes van Buzzoid voor slechts $1.47. Directe levering, echte likes en vriendelijke 24/7 klantenondersteuning. Probeer ons vandaag nog.'
        },
        'buy-instagram-likes-alt': {
            title: 'Koop Instagram Likes - 100% Real & Instant Likes | Nu $1.47',
            description: 'Koop Instagram-likes van Buzzoid voor slechts $1.47. Directe levering, echte likes en vriendelijke 24/7 klantenondersteuning. Probeer ons vandaag nog.'
        },
        'automatic-instagram-likes': {
            title: 'Buy Automatic Instagram Likes - 100% Real &amp; Instant Delivery',
            description: 'We got the competition shaking due to our extremely low prices, unbelievably cheap prices, and amazing customer support. Instagram services just got easier.',
            descriptionOG: 'Buy automatic Instagram likes from Buzzoid with our affordable monthly packages. Instant delivery, real likes and friendly 24/7 customer support. Try us today.'
        },
        'buy-instagram-followers': {
            title: 'Koop Instagram-volgers - 100% Real & Instant | Nu $2.97',
            description: 'Koop Instagram-volgers van Buzzoid voor slechts $2.97. Directe levering, echte volgers en vriendelijke 24/7 klantenondersteuning. Probeer ons vandaag nog.'
        },
        'buy-instagram-followers-alt': {
            title: 'Koop Instagram-volgers - 100% Real & Instant | Nu $2.97',
            description: 'Koop Instagram-volgers van Buzzoid voor slechts $2.97. Directe levering, echte volgers en vriendelijke 24/7 klantenondersteuning. Probeer ons vandaag nog.'
        },
        'buy-instagram-views': {
            title: 'Koop Instagram-weergaven - Real & Instant Delivery | Nu $1.99',
            description: 'Als u Instagram-views wilt kopen, kan dat nu een stuk eenvoudiger dankzij de lage prijzen, snelle bezorgsnelheid en vriendelijke klantendienst van Buzzoid.'
        },
        'buy-instagram-views-alt': {
            title: 'Koop Instagram-weergaven - Real & Instant Delivery | Nu $1.99',
            description: 'Als u Instagram-views wilt kopen, kan dat nu een stuk eenvoudiger dankzij de lage prijzen, snelle bezorgsnelheid en vriendelijke klantendienst van Buzzoid.'
        },
        'faq': {
            title: 'Veelgestelde vragen - Buzzoid',
            description: "Vragen die u wellicht wilt stellen en al eerder zijn gesteld. Lees ze door om er zeker van te zijn dat u up-to-date bent."
        },
        'contact-us': {
            title: 'Neem contact met ons op - Buzzoid',
            description: undefined
        },
        'blog': {
            title: 'Instagram Marketing Blog: Strategies, Tips & Guides | Buzzoid',
            description: 'Get the freshest and latest news about Instagram marketing directly from Buzzoid. We provide you with all the best guides to grow your Instagram account.'
        },
        'posts': {
            title: undefined,
            description: undefined
        },
        'terms-of-service': {
            title: 'Terms of Service - Buzzoid',
            description: 'Terms of use Thanks for using our products and services (“Services”). The Services are provided by Perago LLC, +1 855-848-9812. By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any …'
        },
        'privacy-policy': {
            title: 'Privacy Policy - Buzzoid',
            description: 'Privacy Policy We take your privacy seriously and will take all measures to protect your personal information. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. Any personal information received will only be used to fill your order. We …'
        },
    },
    'pt': {
        'home': {
            title: 'Buzzoid - Comprar Instagram Seguidores e Gostos a partir de $2.97',
            description: 'Temos a concorrência abalada devido aos nossos preços extremamente baixos, preços inacreditavelmente baratos e incrível suporte ao cliente. Os serviços do Instagram ficaram mais fáceis.'
        },
        'buy-instagram-likes': {
            title: 'Comprar curtidas no Instagram - 100% curtidas reais e instantâneas | Agora $1.47',
            description: 'Compre curtidas no Instagram do Buzzoid por apenas $1.47. Entrega instantânea, curtidas reais e suporte ao cliente amigável 24 horas por dia, 7 dias por semana. Experimente-nos hoje.'
        },
        'buy-instagram-likes-alt': {
            title: 'Comprar curtidas no Instagram - 100% curtidas reais e instantâneas | Agora $1.47',
            description: 'Compre curtidas no Instagram do Buzzoid por apenas $1.47. Entrega instantânea, curtidas reais e suporte ao cliente amigável 24 horas por dia, 7 dias por semana. Experimente-nos hoje.'
        },
        'automatic-instagram-likes': {
            title: 'Buy Automatic Instagram Likes - 100% Real &amp; Instant Delivery',
            description: 'We got the competition shaking due to our extremely low prices, unbelievably cheap prices, and amazing customer support. Instagram services just got easier.',
            descriptionOG: 'Buy automatic Instagram likes from Buzzoid with our affordable monthly packages. Instant delivery, real likes and friendly 24/7 customer support. Try us today.'
        },
        'buy-instagram-followers': {
            title: 'Compre Seguidores do Instagram - 100% Real e Instantâneo | Agora $2.97',
            description: 'Compre seguidores do Instagram da Buzzoid por apenas $2.97. Entrega instantânea, seguidores reais e suporte ao cliente amigável 24 horas por dia, 7 dias por semana. Experimente-nos hoje.'
        },
        'buy-instagram-followers-alt': {
            title: 'Compre Seguidores do Instagram - 100% Real e Instantâneo | Agora $2.97',
            description: 'Compre seguidores do Instagram da Buzzoid por apenas $2.97. Entrega instantânea, seguidores reais e suporte ao cliente amigável 24 horas por dia, 7 dias por semana. Experimente-nos hoje.'
        },
        'buy-instagram-views': {
            title: 'Compre visualizações no Instagram - entrega real e instantânea | Agora $1.99',
            description: 'Se você quiser comprar visualizações do Instagram, as coisas estão prestes a ficar muito mais fáceis. A Buzzoid tem preços competitivos, velocidade de entrega rápida e suporte amigável.'
        },
        'buy-instagram-views-alt': {
            title: 'Compre visualizações no Instagram - entrega real e instantânea | Agora $1.99',
            description: 'Se você quiser comprar visualizações do Instagram, as coisas estão prestes a ficar muito mais fáceis. A Buzzoid tem preços competitivos, velocidade de entrega rápida e suporte amigável.'
        },
        'faq': {
            title: 'Perguntas frequentes - Buzzoid',
            description: "Perguntas que você pode querer fazer e que já foram feitas antes. Leia para se certificar de que está na página certa."
        },
        'contact-us': {
            title: 'Entre em contato - Buzzoid',
            description: undefined
        },
        'blog': {
            title: 'Instagram Marketing Blog: Strategies, Tips & Guides | Buzzoid',
            description: 'Get the freshest and latest news about Instagram marketing directly from Buzzoid. We provide you with all the best guides to grow your Instagram account.'
        },
        'posts': {
            title: undefined,
            description: undefined
        },
        'terms-of-service': {
            title: 'Terms of Service - Buzzoid',
            description: 'Terms of use Thanks for using our products and services (“Services”). The Services are provided by Perago LLC, +1 855-848-9812. By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any …'
        },
        'privacy-policy': {
            title: 'Privacy Policy - Buzzoid',
            description: 'Privacy Policy We take your privacy seriously and will take all measures to protect your personal information. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. Any personal information received will only be used to fill your order. We …'
        },
    }
};

export default metaTagsMap;
