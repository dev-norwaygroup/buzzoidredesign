const likesSchema = {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "name": "FAQ",
    "mainEntity": [
        {
            "@type": "Question",
            "name": "Why should I buy Instagram likes?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Instagram likes aren't just a vanity metric — the number of likes you get directly affects Instagram's core algorithm. The more likes and engagement your content has, the more people you're going to reach.\nBuying likes is the single best way of boosting your presence on social media — earning you more recognition, more followers, and ultimately, more conversions.\nThe more likes a photo or video receives, the higher the chance of reaching the explore page — opening you up to millions of new viewers.\nLikes also serve as social proof for viewers that see your content. When a photo has a high number of likes, they're more likely to engage with it further. Buying likes is a good way to catalyze this interaction — boosting the organic engagement your content is capable of throughout its lifetime.\nYou can also buy Instagram followers at Buzzoid."
            }
        },
        {
            "@type": "Question",
            "name": "Which Package Should I Choose?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We offer several different packages to fit your brand’s unique marketing goals and budget.\nFirst, you should decide whether you want high-quality likes or premium likes.\nThe high-quality package is a little bit more affordable. It works great for accounts that need a general boost in engagement.\nThe premium package is better for new accounts or those looking to really step their game up and drive more followers and conversions.\nBoth packages come in quantities of 50,100, & 250 per photo or video."
            }
        },
        {
            "@type": "Question",
            "name": "What Information Do I Need to Provide?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "The only information we need is your username and instructions regarding which photo or video you want to receive the likes.\nWe will never ask for your password or any private information about your account. Be wary of companies asking for logins or other sensitive information when buying Instagram likes online — this simply isn’t needed to roll out this type of service."
            }
        },
        {
            "@type": "Question",
            "name": "Why Choose Buzzoid?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Buzzoid was formed by a team of digital media experts with over a decade of experience operating social media accounts.\nWe’re constantly testing new methods of driving social engagement and know what works and what doesn’t. We’ve grown thousands of accounts and delivered millions of likes over the past several years.\nWe're proud to report more than 1000 recurring monthly customers that using Buzzoid to grow their social media presence.\nNeed social proof? Check our reviews page to see what our past customers are saying about our service."
            }
        },
        {
            "@type": "Question",
            "name": "How Fast Is Your Turnaround Time?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We don't waste any time. Our system will process your order within minutes. However, our users are 100% authentic, so it can take up to 24 hours to roll out completely. \nWe've also learned to avoid Instagram's spam filter by matching the velocity of our likes roll out depending on where the accounts are registered. This is a sophisticated process, but it works. There's no point buying likes if it's going to penalize your content and slow your growth. This is why we prefer to be methodical in how the likes are rolled out depending on the timezone in which most of your userbase is registered."
            }
        }
    ]
};

const followersSchema = {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "name": "FAQ",
    "mainEntity": [
        {
            "@type": "Question",
            "name": "Why Should I Buy Instagram Followers?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "The number of followers you have means a lot more than you might expect. Most viewers take the number of followers on an account into consideration before they decide to click the follow button — or not.\nThe perception of popularity is often enough to manifest this quality in real life. You could be an expert in your field, but without enough followers to \"prove\" it, nobody is going to pay any attention to you. \nWhether you’re a new account trying to get off the ground faster, or a mature account in need of a boost, there’s plenty of reasons why it makes sense to buy new followers from Buzzoid.\nYou can also buy Instagram likes from Buzzoid."
            }
        },
        {
            "@type": "Question",
            "name": "What’s the Quality of Buzzoid Followers?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "It's one thing to get thousands of new followers, but if they're entirely fake, you could be flagged for breaking Instagram's terms.\nThis is the primary difference between high-quality Instagram followers and cheap or low-quality followers.\nHere at Buzzoid, we don’t even bother with low-quality followers. These followers have very low engagement with the platform and are often booted off Instagram after a couple of weeks.\nThis is referred to as \"drop-off.\" You may get a boost of followers in the short term, but they quickly fall off as the accounts are deleted.\n\nWe have two tiers to choose from:\nHigh-Quality Followers — Followers with profile pictures, but no further uploads.\nPremium Followers — Followers with profile pictures and regularly posted content.\n\nWe’ve developed a system of generating authentic followers that work to boost your following without leading to a drop-off a few weeks later. This is a common experience users report after buying cheap Instagram followers. Drop-off is still a (rare) possibility in our ecosystem. We’ll replace any drop-off followers within 30 days of your order. "
            }
        },
        {
            "@type": "Question",
            "name": "What’s The Turnaround Time After I Place My Order?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "When you place an order, our system automatically begins assigning you followers. In order to prevent a dramatic influx of followers that could trigger Instagram's spam detection, we roll out your new followers over a couple of days. The rate of dishing out new followers depends on the size of your current audience.\nThe more followers you have, the faster we can roll out your order. Most rollouts are complete within 48 hours after your purchase. "
            }
        },
        {
            "@type": "Question",
            "name": "Why Choose Buzzoid? ",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Buzzoid was created by a team of social media experts with over 12 years of experience on social media platforms. We’re constantly testing and improving our process to stay one step ahead of the competition.\nWe're constantly running tests within the Instagram ecosystem. This allows us to find the optimal follower velocity when rolling out new orders. Our system leverages real users — so you're not going to run into problems with Instagram over their terms and conditions.\nThe overall impact of your new followers is going to have substantially greater results than bot-driven services.\nIf that’s not enough, our 1000 satisfied monthly recurring customers says it all. "
            }
        },
        {
            "@type": "Question",
            "name": "Could My Account Be Banned For Buying Followers?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Buying cheap, low-quality followers comes with a high chance of getting your account flagged or banned for good. This is the sole reason we don’t even dabble in low-quality, spammy followers.\nAll our followers are authentic users, so you’re not going to be banned for using our service.\nWe’ve been doing this for years and have grown thousands of accounts with this method. Throughout all of these projects, we have yet to discover a single case of an account that was banned as a result of buying followers from Buzzoid. "
            }
        },
        {
            "@type": "Question",
            "name": "Which Package Should I Choose?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We offer several different packages to fit your brand’s unique requirements.\nFirst, you should decide whether you want high-quality followers or premium followers.\nThe high-quality package is best for accounts that already have a decent following and want a little boost. The premium package is for those serious about growing their accounts to scale or brand new accounts with less than 5000 followers. The premium package has an exceptionally low drop-off lasting several years after your purchase.\nBoth packages come in quantities of 500, 1000, 2500, or 5000 new followers per purchase."
            }
        },
        {
            "@type": "Question",
            "name": "What Information Do I Need to Provide?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We don’t need much — just your username.\nWe will never ask for your password or any personal or private information about your account.\nWe accept the usual forms of payment, including all major credit cards and PayPal (coming soon). "
            }
        }
    ]
};

const viewsSchema = {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "name": "FAQ",
    "mainEntity": [
        {
            "@type": "Question",
            "name": "Why should I buy views?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "People by nature are social creatures. This phenomenon is particularly evident in our use of social media platforms. With over 300 million monthly users on Insta, social media is more active today than ever before. That means that your Instagram profile and content is extremely important. If your video only has a view count of a few views, people aren't likely to see it as worth watching. But if you buy Instagram views and your video looks popular, more and more people will want to check it out (and it might even hit the explore page!)"
            }
        },
        {
            "@type": "Question",
            "name": "How qualitative are your views?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We offer only a high-quality, real views service. Some of our competitors resort to techniques such as robots, or computer codes to keep their costs low. We'd never do that to you! When you buy Instagram views from us, we provide you with the best views money can buy. We're proud to offer both quality and quantity, and it's one of the reasons why the world's leading social media influencers use Buzzoid."
            }
        },
        {
            "@type": "Question",
            "name": "What do I need to provide you with?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We like to make this process as easy as possible on our customers, so all we need from you is your Instagram username. Other service providers might ask you to spend a lot of time signing up and entering a lot of personal information like passwords and security questions. We hate that kind of hassle, so we keep it simple. All you need to do is pick your number of views, enter your username, send a quick payment, and you'll be all set!"
            }
        },
        {
            "@type": "Question",
            "name": "Can you deliver exactly when I upload?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Of course! We hate waiting, and we're sure you do too. Plus, we know that the best time to attract more viewers is in when your video is first uploaded, and our goal is to help you out. So we won't make you wait. As soon as you upload your video, buy Instagram views and we'll deliver them instantly. This is also a great way for Instagram users and marketers to get new followers too!"
            }
        },
        {
            "@type": "Question",
            "name": "Why choose us?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We're the best in the business for a reason! Our team is composed of elite social media marketing experts with over 12 years of experience. We've been providing Instagram services ever since Instagram launched, and we've always prided ourselves on our customers' satisfaction and success. Over 1,000,000 satisfied customers can't be wrong! Plus, we offer a money-back guarantee. If for whatever reason you're not satisfied with your purchase, we'll give you a 100% refund. So there's nothing to lose!  "
            }
        },
        {
            "@type": "Question",
            "name": "Can I get my account banned?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "No way. When you buy Instagram views from us, your account is safe. Our goal is to help your Instagram account grow, not get it banned. Our team adheres to Instagram's terms and conditions, so you're never putting your account at risk. So what are you waiting for? Choose one of the packages listed above and get started today."
            }
        }
    ]
};

const faqSchema = {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "name": "FAQ",
    "mainEntity": [
        {
            "@type": "Question",
            "name": "I did not receive my order, what do I do now?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Please ensure the following criteria are met before placing an order to secure a successful delivery.\n Your Instagram account is set to public.\n You have typed in the correct instagram username.\nYou have not changed your Instagram username before the order's completion.\n The account or post, still exists and has not been deleted.\nThe video follows Instagram's guidelines and has not been restricted or removed.\n Allow 24 hours for completion of order delivery.\n If you have experienced an issue with the delivery of your order, please contact our support team for assistance at support@buzzoid.com"
            }
        },
        {
            "@type": "Question",
            "name": "Why are my followers dropping?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Occasionally Instagram will delete accounts that users mark as spam. This causes follower counts to drop all across Instagram. Sometimes it impacts our customers, sometimes it doesn't. If you experience drops within first 30 days of your purchase, we will happily refill your followers in accordance with our free refill policy."
            }
        },
        {
            "@type": "Question",
            "name": "I no longer want the followers I purchased, can they be removed?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "All followers delivered are real from genuine Instagram accounts. Due to this benefit our team cannot transfer or remove followers once they are delivered.\n As the Instagram account owner, you may remove selected followers from your follower list by blocking each individual user."
            }
        },
        {
            "@type": "Question",
            "name": "How do I stop follower replenishment?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": " If you no longer wish to receive replenishment to your account, simply let us know by emailing our team at support@buzzoid.com"
            }
        },
        {
            "@type": "Question",
            "name": "Why choose us?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "We're the best in the business for a reason! Our team is composed of elite social media marketing experts with over 12 years of experience. We've been providing Instagram services ever since Instagram launched, and we've always prided ourselves on our customers' satisfaction and success. Over 1,000,000 satisfied customers can't be wrong! Plus, we offer a money-back guarantee. If for whatever reason you're not satisfied with your purchase, we'll give you a 100% refund. So there's nothing to lose!  "
            }
        },
        {
            "@type": "Question",
            "name": "How often does replenishment occur?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Replenishment will be delivered to your account once every 24 hours, whenever a drop is detected by our system."
            }
        },
        {
            "@type": "Question",
            "name": "How many followers can I buy?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "You may buy Instagram followers as many times as required as long as your orders do not surpass 100,000 followers within a 30 day time frame."
            }
        },
        {
            "@type": "Question",
            "name": "How many likes can I buy",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "You may buy Instagram likes as many times as required as long as your orders do not surpass 100,000 likes within a 30 day time frame."
            }
        },
        {
            "@type": "Question",
            "name": "What are acceptable methods of payment?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Buzzoid products can be easily purchased with any major credit card! Cards accepted at this time are MasterCard, Visa, Visa Electron, American Express and Discover."
            }
        },
        {
            "@type": "Question",
            "name": "How do I pay if I do not have a credit card?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "If you do not have a credit card, you can fill-up a prepaid credit card and use your new prepaid credit card for your Buzzoid order!"
            }
        },
        {
            "@type": "Question",
            "name": "Do you accept PayPal?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Buzzoid no longer accepts PayPal as a payment method. However, If you would like to continue using PayPal&apos;s digital wallet, PayPal offers the PayPal Prepaid Mastercard which you can refill using the funds from your PayPal account."
            }
        },
        {
            "@type": "Question",
            "name": "My credit card was declined, what do I do now?",
            "answerCount": 1,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "When a credit card is declined, our leading recommendation is to contact you bank for more assistance as they may have more insight into what may be causing the issue.\n If you have no luck resolving the issue with your banking institution, please contact our friendly support team at support@buzzoid.com"
            }
        },
    ]
};

export  {
    faqSchema,
    viewsSchema,
    likesSchema,
    followersSchema
}
