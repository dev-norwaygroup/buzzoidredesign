/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal Dependencies
 */
import TranslatedLink from '@/components/translated-link/translated-link';

const defaultItems = [
  {
    title: <Trans>I did not receive my order, what do I do now?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            Please ensure the following criteria are met before placing an order
            to secure a successful delivery.
          </Trans>
        </p>
        <ul>
          <li>
            <Trans>Your </Trans><b>{' '}<Trans>Instagram</Trans></b> <Trans>account is set to</Trans> <b><Trans>public</Trans></b>.
          </li>
          <li>
            <Trans>You have typed in the </Trans><b>{' '}<Trans> correct Instagram username</Trans></b>.
          </li>
          <li>
            <Trans>You have </Trans><b> <Trans>not changed your Instagram username </Trans></b> <Trans> before the
              order&apos;s completion.</Trans>{' '}
          </li>
          <li>
            <Trans>The </Trans><b>{' '}<Trans>account or post</Trans></b>, <Trans>still exists and has</Trans>{' '}
            <b><Trans>not been deleted</Trans></b>.{' '}
          </li>
          <li>
            <Trans>The video follows Instagram&apos;s guidelines and has</Trans>{' '}
            <b><Trans> not been restricted</Trans></b> <Trans>or removed.</Trans>{' '}
          </li>
          <li>
            <b><Trans>Allow 24 hours </Trans></b> <Trans> for completion of order delivery.</Trans>
          </li>
        </ul>

        <p>
          <Trans>
            If you have experienced an issue with the delivery of your order,
            please contact our support team for assistance at
          </Trans>{' '}
          <a href="mailto:support@buzzoid.com">
            support@buzzoid.com
          </a>
        </p>
      </>
    ),
  },
  {
    title: <Trans>Why are my followers dropping?</Trans>,
    content: (
      <p>
        <Trans>
          Occasionally Instagram will delete accounts that users mark as spam.
          This causes follower counts to drop all across Instagram. Sometimes it
          impacts our customers, sometimes it doesn&apos;t. If you experience
          drops within first 30 days of your purchase, we will happily refill
          your followers in accordance with our free refill policy.
        </Trans>
      </p>
    ),
  },
  {
    title: (
      <Trans>
        I no longer want the followers I purchased, can they be removed?
      </Trans>
    ),
    content: (
      <>
        <p>
          <Trans>
            All followers delivered are real from genuine Instagram accounts.
            Due to this benefit our team cannot transfer or remove followers
            once they are delivered.
          </Trans>
        </p>
        <p>
          <Trans>
            As the Instagram account owner, you may remove selected followers
            from your follower list by blocking each individual user.
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>How do I stop follower replenishment?</Trans>,
    content: (
      <p>
        <Trans>
          {' '}
          If you no longer wish to receive replenishment to your account, simply
          let us know by emailing our team at
        </Trans>{' '}
        <a href="mailto:support@buzzoid.com">
          support@buzzoid.com
        </a>
      </p>
    ),
  },
  {
    title: <Trans>How often does replenishment occur?</Trans>,
    content: (
      <p>
        <Trans>
          Replenishment will be delivered to your account once every 24 hours,
          whenever a drop is detected by our system.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>How many followers can I buy?</Trans>,
    content: (
      <p>
        <Trans>You may </Trans>{' '}
        <TranslatedLink href="/buy-instagram-followers/">
          <a>
            <Trans>buy Instagram followers</Trans>
          </a>
        </TranslatedLink>{' '}
        <Trans>
          as many times as required as long as your orders do not surpass
          100,000 followers within a 30 day time frame.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>How many likes can I buy</Trans>,
    content: (
      <p>
        <Trans>You may </Trans>{' '}
        <TranslatedLink href="/buy-instagram-likes/">
          <a>
            <Trans>buy Instagram likes</Trans>
          </a>
        </TranslatedLink>{' '}
        <Trans>
          as many times as required as long as your orders do not surpass
          100,000 likes within a 30 day time frame.
        </Trans>
      </p>
    ),
  },
  {
    title: (
      <Trans>
        What are acceptable methods of payment?
      </Trans>
    ),
    content: (
      <p>
        <Trans>
          Buzzoid products can be easily purchased with any major credit card! Cards accepted at this time are MasterCard, Visa, Visa Electron, American Express and Discover.
        </Trans>
      </p>
    ),
  },
  {
    title: (
      <Trans>
        How do I pay if I do not have a credit card?
      </Trans>
    ),
    content: (
      <p>
        <Trans>
          If you do not have a credit card, you can fill-up a prepaid credit card and use your new prepaid credit card for your Buzzoid order!
        </Trans>
      </p>
    ),
  },
  {
    title: (
      <Trans>
        Do you accept PayPal?
      </Trans>
    ),
    content: (
      <p>
        <Trans>
          Buzzoid no longer accepts PayPal as a payment method. However, If you would like to continue using PayPal&apos;s digital wallet, PayPal offers the PayPal Prepaid Mastercard which you can refill using the funds from your PayPal account.
        </Trans>
      </p>
    ),
  },
  {
    title: (
      <Trans>
        My credit card was declined, what do I do now?
      </Trans>
    ),
    content: (
      <>
        <p>
          <Trans>
            When a credit card is declined, our leading recommendation is to contact you bank for more assistance as they may have more insight into what may be causing the issue.
          </Trans>
        </p>
        <p>
          <Trans>If you have no luck resolving the issue with your banking institution, please contact our friendly support team at</Trans>{' '}
          <a href="mailto:support@buzzoid.com">
            support@buzzoid.com
          </a>
        </p>
      </>
    ),
  },
];

const buyLikesItems = [
  {
    title: <Trans>Why should I buy instagram likes?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            Instagram likes aren&apos;t just a vanity metric — the number of
            likes you get directly affects Instagram&apos;s core algorithm. The
            more likes and engagement your content has, the more people
            you&apos;re going to reach.
          </Trans>
        </p>
        <p>
          <Trans>
            Buying likes is the single best way of boosting your presence on
            social media — earning you more recognition, more followers, and
            ultimately, more conversions.
          </Trans>
        </p>
        <p>
          <Trans>
            The more likes a photo or video receives, the higher the chance of
            reaching the explore page — opening you up to millions of new
            viewers.
          </Trans>
        </p>
        <p>
          <Trans>
            Likes also serve as social proof for viewers that see your content.
            When a photo has a high number of likes, they&apos;re more likely to
            engage with it further. Buying likes is a good way to catalyze this
            interaction — boosting the organic engagement your content is
            capable of throughout its lifetime.
          </Trans>
        </p>
        <p>
          <Trans>You can also </Trans>{' '}
          <TranslatedLink href="/buy-instagram-followers/">
            <a>
              <Trans>buy Instagram followers</Trans>
            </a>
          </TranslatedLink>
          {' '}<Trans> at Buzzoid.</Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>Which package should I choose?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            We offer several different packages to fit your brand&apos;s unique
            marketing goals and budget.
          </Trans>
        </p>
        <p>
          <Trans>
            First, you should decide whether you want high-quality likes or
            premium likes.
          </Trans>
        </p>
        <p>
          <Trans>
            The high-quality package is a little bit more affordable. It works
            great for accounts that need a general boost in engagement.
          </Trans>
        </p>
        <p>
          <Trans>
            The premium package is better for new accounts or those looking to
            really step their game up and drive more followers and conversions.
          </Trans>
        </p>
        <p>
          <Trans>
            Both packages come in quantities of 50, 100, &amp; 250 per photo or
            video.
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>What information do I need to provide?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            The only information we need is your username and instructions
            regarding which photo or video you want to receive the likes.
          </Trans>
        </p>
        <p>
          <Trans>
            We will never ask for your password or any private information about
            your account. Be wary of companies asking for logins or other
            sensitive information when buying Instagram likes
            online&nbsp;—&nbsp;this simply isn&apos;t needed to roll out this
            type of service.
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>Why choose Buzzoid</Trans>,
    content: (
      <>
        <p>
          <Trans>
            Buzzoid was formed by a team of digital media experts with over a
            decade of experience operating social media accounts.
          </Trans>
        </p>
        <p>
          <Trans>
            We&apos;re constantly testing new methods of driving social
            engagement and know what works and what doesn&apos;t. We&apos;ve
            grown thousands of accounts and delivered millions of likes over the
            past several years.
          </Trans>
        </p>
        <p>
          <Trans>
            We&apos;re proud to report more than 1000 recurring monthly
            customers that using Buzzoid to grow their social media presence.
          </Trans>
        </p>
        <p>
          <Trans>Need social proof? Check our</Trans>{' '}
          <TranslatedLink href="/reviews/">
            <a>
              <Trans>reviews</Trans>
            </a>
          </TranslatedLink>{' '}
          <Trans>
            page to see what our past customers are saying about our service.{' '}
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>How fast is your turnaround time?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            We don&apos;t waste any time. Our system will process your order
            within minutes. However, our users are 100% authentic, so it can
            take up to 24 hours to roll out completely.{' '}
          </Trans>
        </p>
        <p>
          <Trans>
            We&apos;ve also learned to avoid Instagram&apos;s spam filter by
            matching the velocity of our likes roll out depending on where the
            accounts are registered. This is a sophisticated process, but it
            works. There&apos;s no point buying likes if it&apos;s going to
            penalize your content and slow your growth. This is why we prefer to
            be methodical in how the likes are rolled out depending on the
            timezone in which most of your userbase is registered.{' '}
          </Trans>
        </p>
      </>
    ),
  },
];

const buyFollowersItems = [
  {
    title: <Trans>Why should I buy instagram followers?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            The number of followers you have means a lot more than you might
            expect. Most viewers take the number of followers on an account into
            consideration before they decide to click the follow button — or
            not.
          </Trans>
        </p>
        <p>
          <Trans>
            The
          </Trans>{' '}
          <i><Trans>perception</Trans></i> <Trans>of popularity is often enough to manifest this
            quality in real life. You could be an expert in your field, but
            without enough followers to &quot;prove&quot; it, nobody is going to
            pay any attention to you</Trans>.{' '}
        </p>
        <p>
          <Trans>
            Whether you&apos;re a new account trying to get off the ground
            faster, or a mature account in need of a boost, there&apos;s plenty
            of reasons why it makes sense to buy new followers from Buzzoid.
          </Trans>
        </p>
        <p>
          <Trans>You can also</Trans>{' '}
          <TranslatedLink href="/buy-instagram-likes/">
            <a>
              <Trans> buy Instagram likes</Trans>
            </a>
          </TranslatedLink>
          {' '}<Trans> from Buzzoid.</Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>What&apos;s the quality of Buzzoid followers?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            It&apos;s one thing to get thousands of new followers, but if
            they&apos;re entirely fake, you could be flagged for breaking
            Instagram&apos;s terms.
          </Trans>
        </p>
        <p>
          <Trans>
            This is the primary difference between high-quality Instagram
            followers and cheap or low-quality followers.{' '}
          </Trans>
        </p>
        <p>
          <Trans>
            Here at Buzzoid, we don&apos;t even bother with low-quality
            followers. These followers have very low engagement with the
            platform and are often booted off Instagram after a couple of weeks.{' '}
          </Trans>
        </p>
        <p>
          <Trans>
            This is referred to as &quot;drop-off.&quot; You may get a boost
            of followers in the short term, but they quickly fall off as the
            accounts are deleted.{' '}
          </Trans>
        </p>
        <p>
          <b><Trans>We have two tiers to choose from:</Trans></b>
        </p>
        <ul>
          <li>
            <b><Trans>High-Quality Followers</Trans></b>{' '}<Trans>— Followers with profile pictures,
              but no further uploads.</Trans>
          </li>
          <li>
            <b><Trans>Premium Followers</Trans></b>{' '}<Trans>— Followers with profile pictures and
              regularly posted content.
            </Trans>
          </li>
        </ul>
        <p>
          <Trans>
            We&apos;ve developed a system of generating authentic followers that
            work to boost your following without leading to a drop-off a few
            weeks later. This is a common experience users report after buying
            cheap Instagram followers. Drop-off is still a &#40;rare&#41;
            possibility in our ecosystem. We&apos;ll replace any drop-off
            followers within 30 days of your order.{' '}
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: (
      <Trans>What&apos;s the turnaround time after I place my order?</Trans>
    ),
    content: (
      <>
        <p>
          <Trans>
            When you place an order, our system automatically begins assigning
            you followers. In order to prevent a dramatic influx of followers
            that could trigger Instagram&apos;s spam detection, we roll out your
            new followers over a couple of days. The rate of dishing out new
            followers depends on the size of your current audience.
          </Trans>
        </p>
        <p>
          <Trans>
            The more followers you have, the faster we can roll out your order.
            Most rollouts are complete within 48 hours after your purchase.
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>Why choose Buzzoid?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            Buzzoid was created by a team of social media experts with over 12
            years of experience on social media platforms. We&apos;re constantly
            testing and improving our process to stay one step ahead of the
            competition.
          </Trans>
        </p>
        <p>
          <Trans>
            We&apos;re constantly running tests within the Instagram ecosystem.
            This allows us to find the optimal follower velocity when rolling
            out new orders. Our system leverages real users — so you&apos;re not
            going to run into problems with Instagram over their terms and
            conditions.
          </Trans>
        </p>
        <p>
          <Trans>
            The overall impact of your new followers is going to have
            substantially greater results than bot-driven services.
          </Trans>
        </p>
        <p>
          <Trans>
            If that&apos;s not enough, our 1000 satisfied monthly recurring
            customers says it all.
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>Could my account be banned for buying followers?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            Buying cheap, low-quality followers comes with a high chance of
            getting your account flagged or banned for good. This is the sole
            reason we don&apos;t even dabble in low-quality, spammy followers.
          </Trans>
        </p>
        <p>
          <Trans>
            All our followers are authentic users, so you&apos;re not going to
            be banned for using our service.
          </Trans>
        </p>
        <p>
          <Trans>
            We&apos;ve been doing this for years and have grown thousands of
            accounts with this method. Throughout all of these projects, we have
            yet to discover a single case of an account that was banned as a
            result of buying followers from Buzzoid.
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>Which package should I choose?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            We offer several different packages to fit your brand&apos;s unique
            requirements.
          </Trans>
        </p>
        <p>
          <Trans>
            First, you should decide whether you want high-quality followers or
            premium followers.
          </Trans>
        </p>
        <p>
          <Trans>
            The high-quality package is best for accounts that already have a
            decent following and want a little boost. The premium package is for
            those serious about growing their accounts to scale or brand new
            accounts with less than 5000 followers. The premium package has an
            exceptionally low drop-off lasting several years after your
            purchase.
          </Trans>
        </p>
        <p>
          <Trans>
            Both packages come in quantities of 500, 1000, 2500, or 5000 new
            followers per purchase.
          </Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>What information do I need to provide?</Trans>,
    content: (
      <>
        <p>
          <Trans>We don&apos;t need much — just your username.</Trans>
        </p>
        <p>
          <Trans>
            We will never ask for your password or any personal or private
            information about your account.
          </Trans>
        </p>
        <p>
          <Trans>
            We accept the usual forms of payment, including all major credit
            cards and PayPal &#40;coming soon&#41;.
          </Trans>
        </p>
      </>
    ),
  },
];

const buyViewsItems = [
  {
    title: <Trans>Why should I buy views?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            People by nature are social creatures. This phenomenon is
            particularly evident in our use of social media platforms. With over
            300 million monthly users on Insta, social media is more active
            today than ever before. That means that your Instagram profile and
            content is extremely important. If your video only has a view count
            of a few views, people aren&apos;t likely to see it as worth
            watching. But if you buy Instagram views and your video looks
            popular, more and more people will want to check it out &#40;and it
            might even hit the explore page!&#41;
          </Trans>
        </p>
        <p>
          <Trans>Why buy Instagram views from Buzzoid?</Trans>
        </p>
        <ul>
          <li>
            <Trans>Instant delivery</Trans>
          </li>
          <li>
            <Trans>100% real views</Trans>
          </li>
          <li>
            <Trans>24/7 customer support</Trans>
          </li>
        </ul>
      </>
    ),
  },
  {
    title: <Trans>How qualitative are your views?</Trans>,
    content: (
      <p>
        <Trans>
          We offer only a high-quality, real views service. Some of our
          competitors resort to techniques such as robots, or computer codes to
          keep their costs low. We&apos;d never do that to you! When you buy
          Instagram views from us, we provide you with the best views money can
          buy. We&apos;re proud to offer both quality and quantity, and
          it&apos;s one of the reasons why the world&apos;s leading social media
          influencers use Buzzoid.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>What do I need to provide you with?</Trans>,
    content: (
      <p>
        <Trans>
          We like to make this process as easy as possible on our customers, so
          all we need from you is your Instagram username. Other service
          providers might ask you to spend a lot of time signing up and entering
          a lot of personal information like passwords and security questions.
          We hate that kind of hassle, so we keep it simple. All you need to do
          is pick your number of views, enter your username, send a quick
          payment, and you&apos;ll be all set!
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>Can you deliver exactly when I upload?</Trans>,
    content: (
      <p>
        <Trans>
          Of course! We hate waiting, and we&apos;re sure you do too. Plus, we
          know that the best time to attract more viewers is in when your video
          is first uploaded, and our goal is to help you out. So we won&apos;t
          make you wait. As soon as you upload your video, buy Instagram views
          and we&apos;ll deliver them instantly. This is also a great way for
          Instagram users and marketers to get new followers too!
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>Why choose us?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            We&apos;re the best in the business for a reason! Our team is
            composed of elite social media marketing experts with over 12 years
            of experience. We&apos;ve been providing Instagram services ever
            since Instagram launched, and we&apos;ve always prided ourselves on
            our customers&apos; satisfaction and success. Over 1,000,000
            satisfied customers can&apos;t be wrong! Plus, we offer a money-back
            guarantee. If for whatever reason you&apos;re not satisfied with
            your purchase, we&apos;ll give you a 100% refund. So there&apos;s
            nothing to lose!
          </Trans>
        </p>
        <p>
          <Trans>Still not convinced? Check our</Trans>{' '}
          <TranslatedLink href="/reviews/">
            <a>
              <Trans>reviews</Trans>
            </a>
          </TranslatedLink>{' '}
          <Trans>
            page to see what our customers are saying about our quality
            services. You can also
          </Trans>{' '}
          <TranslatedLink href="/buy-instagram-followers/">
            <a>
              <Trans>buy Instagram followers</Trans>
            </a>
          </TranslatedLink>{' '}
          <Trans>and</Trans>{' '}
          <TranslatedLink href="/buy-instagram-likes/">
            <a>
              <Trans>likes</Trans>
            </a>
          </TranslatedLink>{' '}
          <Trans>from Buzzoid.</Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>Can I get my account banned?</Trans>,
    content: (
      <p>
        <Trans>
          No way. When you buy Instagram views from us, your account is safe.
          Our goal is to help your Instagram account grow, not get it banned.
          Our team adheres to Instagram&apos;s terms and conditions, so
          you&apos;re never putting your account at risk. So what are you
          waiting for? Choose one of the packages listed above and get started
          today.
        </Trans>
      </p>
    ),
  },
];

const autoLikesItems = [
  {
    title: <Trans>When will I receive my automatic Instagram likes?</Trans>,
    content: (
      <p>
        <Trans>
          Our system monitors your social media account 24/7 and detects your latest posts within 60 seconds. Likes start delivering to your posts immediately after it is detected.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>Can the likes I receive be spread out instead of delivered all at once?</Trans>,
    content: (
      <p>
        <Trans>
          Absolutely. You can adjust how fast or slow you want to receive likes. Depending on your personal preference, speeds range from Instant to 4 hours.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>Do I need to give you my password?</Trans>,
    content: (
      <p>
        <Trans>
          Absolutely not. Your password is completely confidential. All we require is your Instagram profile username.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>Can my account stay private</Trans>,
    content: (
      <p>
        <Trans>
          No, we cannot detect new posts on private accounts. Therefore, users must have their profiles set to public in order to receive automatic likes.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>How many of my posts will receive automatic likes?</Trans>,
    content: (
      <p>
        <Trans>
          To prevent abuse of our system, automatic like delivery is limited to your first 4 posts per day. Don&apos;t worry, we have alternative options for those who post more often. Simply contact us for details.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>Can I receive likes on my older posts?</Trans>,
    content: (
      <p>
        <Trans>
          We do not automatically send likes to your older posts. However, you may purchase credits through your Buzzoid dashboard and manually send likes to any of your posts, including older ones.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>How do you deliver real likes?</Trans>,
    content: (
      <p>
        <Trans>
          Unlike most other services out there, we </Trans> <strong><Trans>do not</Trans></strong> <Trans>use illegitimate methods to deliver your likes. Whenever you share a new post, we show it to our in-network users in real time. If they&apos;ve opted in to our Like Program, they&apos;ll give your post a like. These are high quality automatic Instagram likes.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>Will my account get banned or disabled?</Trans>,
    content: (
      <p>
        <strong><Trans>No, our system is 100% safe and will not get your account banned or disabled. </Trans></strong> <Trans>Not one of our thousands of customers has ever been banned as a result of using our real Instagram likes or followers services.
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>Do you offer automatic followers?</Trans>,
    content: (
      <p>
        <Trans>
          We are currently working on introducing automatic Instagram followers in the near future, stay tuned!
        </Trans>
      </p>
    ),
  },
  {
    title: <Trans>How do I cancel my subscription?</Trans>,
    content: (
      <>
        <p>
          <Trans>
            Log into your Buzzoid dashboard, go to Subscriptions and click &apos;Manage Subscription&apos;. Once you&apos;ve cancelled your subscription, you will not be billed again.
          </Trans>
        </p>
        <p>
          <Trans>
            Your subscription will remain active until the end of the billing cycle. You can also cancel by</Trans>{' '}
          <TranslatedLink href="/contact-us/">
            <a><Trans>contacting us</Trans>
            </a>
          </TranslatedLink>{' '}
          <Trans>directly.</Trans>
        </p>
      </>
    ),
  },
  {
    title: <Trans>What is your refund policy?</Trans>,
    content: (
      <p>
        <Trans>
          If you are not completely satisfied within the first 30 days of your new subscription, simply notify us and we will refund you - no questions asked.
        </Trans>
      </p>
    ),
  }
]

export { defaultItems, buyLikesItems, buyFollowersItems, buyViewsItems, autoLikesItems };
