const siteMap = {
    'en': {
        '/buy-instagram-likes/': '/buy-instagram-likes/',
        '/buy-instagram-likes-alt/': '/buy-instagram-likes-alt/',
        '/automatic-instagram-likes/': '/automatic-instagram-likes/',
        '/buy-instagram-followers/': '/buy-instagram-followers/',
        '/buy-instagram-followers-alt/': '/buy-instagram-followers-alt/',
        '/buy-instagram-views/': '/buy-instagram-views/',
        '/buy-instagram-views-alt/': '/buy-instagram-views-alt/',
        '/contact-us/': '/contact-us/',
        '/faq/': '/faq/'
    },
    'fr': {
        '/buy-instagram-likes/': '/acheter-instagram-likes/',
        '/buy-instagram-likes-alt/': '/acheter-instagram-likes-alt/',
        '/automatic-instagram-likes/': '/automatic-instagram-likes/',
        '/buy-instagram-followers/': '/acheter-instagram-followers/',
        '/buy-instagram-followers-alt/': '/acheter-instagram-followers-alt/',
        '/buy-instagram-views/': '/acheter-instagram-views/',
        '/buy-instagram-views-alt/': '/acheter-instagram-views-alt/',
        '/contact-us/': '/contactez-nous/',
        '/faq/': '/faq/',
    },

    'de': {
        '/buy-instagram-likes/': '/instagram-likes-kaufen/',
        '/buy-instagram-likes-alt/': '/instagram-likes-kaufen-alt/',
        '/automatic-instagram-likes/': '/automatic-instagram-likes/',
        '/buy-instagram-followers/': '/instagram-followers-kaufen/',
        '/buy-instagram-followers-alt/': '/instagram-followers-kaufen-alt/',
        '/buy-instagram-views/': '/instagram-views-kaufen/',
        '/buy-instagram-views-alt/': '/instagram-views-kaufen-alt/',
        '/contact-us/': '/kontakt/',
        '/faq/': '/faq/',
    },

    'nl': {
        '/buy-instagram-likes/': '/instagram-likes-kopen/',
        '/buy-instagram-likes-alt/': '/instagram-likes-kopen-alt/',
        '/automatic-instagram-likes/': '/automatic-instagram-likes/',
        '/buy-instagram-followers/': '/instagram-followers-kopen/',
        '/buy-instagram-followers-alt/': '/instagram-followers-kopen-alt/',
        '/buy-instagram-views/': '/instagram-views-kopen/',
        '/buy-instagram-views-alt/': '/instagram-views-kopen-alt/',
        '/contact-us/': '/contact-ons/',
        '/faq/': '/faq/',
    },

    'pt': {
        '/buy-instagram-likes/': '/comprar-curtidas-instagram/',
        '/buy-instagram-likes-alt/': '/comprar-curtidas-instagram-alt/',
        '/automatic-instagram-likes/': '/automatic-instagram-likes/',
        '/buy-instagram-followers/': '/comprar-seguidores-instagram/',
        '/buy-instagram-followers-alt/': '/comprar-seguidores-instagram-alt/',
        '/buy-instagram-views/': '/comprar-views-instagram/',
        '/buy-instagram-views-alt/': '/comprar-views-instagram-alt/',
        '/contact-us/': '/contato/',
        '/faq/': '/faq/',
    }
}

export default siteMap;