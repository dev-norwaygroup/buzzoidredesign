/**
 * External dependencies
 */
import { useRouter } from 'next/router';

/**
 * Internal Dependencies
 */
import Header from '@/components/header/header';
import Footer from '@/components/footer/footer';
import Callout from '@/components/callout/callout';
import Head from 'next/head';
import metaTags from '../statics/meta-tags';
import generateSchema from 'utils/schema';
import { getTranslatedPath } from 'utils/utils';
import siteMap from 'statics/site-map';

const localeMap = {
  'fr': 'fr-FR',
  'en': 'en-US',
  'de': 'de-DE',
  'nl': 'nl-NL',
  'pt': 'pt-BR',
};

const Layout = ({ children }) => {
  const router = useRouter();
  const locale = router.locale;
  const match = router.asPath.match(/\/([\w-]+)/);
  let uniqueClass = match ? match.pop() : 'home';
  let cannonicalPath = `https://buzzoid.com${router.locale != 'en' ? '/' + router.locale : ''}${router.asPath}`;
  let metaTagInstance;

  if (locale != 'en' && uniqueClass != 'home' && uniqueClass) {
    uniqueClass = getTranslatedPath(router.asPath)?.replace(/\//g, '');
    cannonicalPath = `https://buzzoid.com${router.locale != 'en' ? '/' + router.locale : ''}${siteMap[locale][`/${uniqueClass}/`]}`;
  }

  metaTagInstance = metaTags[router.locale][uniqueClass];

  if (uniqueClass == '500') {
    return <>{children}</>
  }

  return (
    <div className={uniqueClass}>
      {
        metaTagInstance && (
          <Head>
            {metaTagInstance.title && <title>{metaTagInstance.title}</title>}
            <link rel="canonical" href={cannonicalPath} />
            <meta property="og:locale" content={localeMap[locale]} />
            <meta property="og:site_name" content="Buzzoid" />
            <meta property="og:url" content={cannonicalPath} />
            <meta property="og:type" content={uniqueClass === 'home' ? 'website' : 'article'} />
            {metaTagInstance.description && <meta name="description" content={metaTagInstance.description} />}
            <meta property="og:title" content={metaTagInstance.title} />
            {
              metaTagInstance.description && <meta property="og:description" content={metaTagInstance.descriptionOG
                ? metaTagInstance.descriptionOG
                : metaTagInstance.description}
              />
            }
            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{
                __html: JSON.stringify(generateSchema(
                  metaTagInstance.title,
                  metaTagInstance.description,
                  localeMap[locale],
                  cannonicalPath,
                  metaTags[uniqueClass].datePublished,
                  metaTags[uniqueClass].dateModified,
                  locale
                )),
              }}
            ></script>
          </Head>
        )
      }
      <Callout />
      <Header />
      <div className="main-wrapper">{children}</div>
      <Footer />
    </div>
  );
};
export default Layout;
