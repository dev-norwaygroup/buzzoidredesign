/**
 * External Dependencies
 */
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useState, useEffect } from 'react';
import { Trans } from '@lingui/macro';
import { useRouter } from 'next/router';

/**
 * Internal dependencies
 */
import CustomerRated from '@/components/customer-rated/customer-rated';
import FlexRow from '@/components/flex-row/flex-row';

import IconChevronDown from '@/svg/icon-chevron-down.svg';
import BuyFormRadio from './buy-form-radio';
import BuyFormInlineRadio from './buy-form-inline-radio';

const orderBaseUrl = process.env.NEXT_PUBLIC_ORDER_API_BASE_URL;
const subscriptionBaseUrl = process.env.NEXT_PUBLIC_SUBSCRIPTION_API_BASE_URL;

const BuyForm = ({ fields, schema, prefix }) => {
  const [formFields, setFormFields] = useState(fields);
  const [toggle, setToggle] = useState(true);
  const [orderCount, setOrderCount] = useState();
  const [isTooltipOpen, setIsTooltipOpen] = useState(false);
  const closeTooltip = () => setIsTooltipOpen(false);
  const toggleTooltip = () => setIsTooltipOpen(!isTooltipOpen);

  const router = useRouter();

  const handleToggleClick = () => {
    if (isTooltipOpen) {
      setIsTooltipOpen(false);
    }
  };

  useEffect(() => {
    if (typeof window === 'object') {
      window.addEventListener('click', handleToggleClick);
    }
    return () => window.removeEventListener('click', handleToggleClick);
  });

  const defaultValues = fields.reduce((acc, val) => {
    if (val.value.filter((item) => (item.checked ? true : false))) {
      return {
        ...acc,
        [val.name]: val.value.filter((item) => item.checked)[0]?.value,
      };
    }
    return acc;
  }, {});
  const {
    control,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    defaultValues,
    resolver: yupResolver(schema),
  });
  const showAll = () => {
    setToggle(false);
    setFormFields((arr) => {
      return arr.map((item) => {
        if (item.type === 'radios') {
          return {
            ...item,
            value: item.value.map((option) => {
              return {
                ...option,
                visible: true,
              };
            }),
          };
        }
        return item;
      });
    });
  };

  const onSubmit = (data) => {
    let newData = data;
    const isTracked = formFields.find((item) => item.track);
    const url = data.automaticLikes ? `${subscriptionBaseUrl}${orderCount}` : `${orderBaseUrl}/${orderCount}`;

    if (isTracked) {
      router.push(url, url);
      return newData;
    }

    const conditionalField = formFields.find((item) => !item.condition);

    if (conditionalField) {
      const conditionalFieldName = conditionalField.name;
      const conditionalFieldValue = data[conditionalFieldName];
      const fieldToRemove = formFields.find(
        (item) =>
          item.condition && item.condition.value != conditionalFieldValue
      );
      const fieldToRemoveName = fieldToRemove.name;
      delete newData[fieldToRemoveName];
    }

    router.push(url, url);
    return;
  };

  return (
    <div className="form buy-form">
      <form onSubmit={handleSubmit(onSubmit)}>
        {formFields.map((item) => {
          switch (item.type) {
            case 'radios':
              if (item.condition) {
                const typeWatch = watch(item.condition.type);
                if (typeWatch != item.condition.value) {
                  return;
                }
              }
              return (
                <Controller
                  key={item.name}
                  render={({ field }) => (
                    <div>
                      <BuyFormRadio
                        prefix={prefix}
                        fields={item.value}
                        fieldValue={field.value}
                        name={item.name}
                        onChange={field.onChange}
                        setOrderCount={setOrderCount}
                      />
                      {errors[item.name]?.message && (
                        <p className="error">{errors[item.name].message}</p>
                      )}
                    </div>
                  )}
                  control={control}
                  name={item.name}
                />
              );
            case 'inline-radios':
              return (
                <Controller
                  key={item.name}
                  render={({ field }) => (
                    <div>
                      <BuyFormInlineRadio
                        toggleTooltip={toggleTooltip}
                        isTooltipOpen={isTooltipOpen}
                        fields={item.value}
                        name={item.name}
                        onChange={(e) => {
                          field.onChange(e);
                          closeTooltip();
                        }}
                      />
                      {errors[item.name]?.message && (
                        <p className="error">{errors[item.name].message}</p>
                      )}
                    </div>
                  )}
                  control={control}
                  name={item.name}
                />
              );
            default:
              return;
          }
        })}
        {toggle && (
          <button type="button" className="radio-toggle" onClick={showAll}>
            <span className="radio-toggle__chevron">
              <IconChevronDown />
            </span>
            <Trans>All Packages</Trans>
            <span className="radio-toggle__orange"><Trans>Save up to</Trans> 75%</span>
          </button>
        )}

        <FlexRow className="form__actions align-center">
          <FlexRow.Col>
            <button type="submit" className="btn btn--orange">
              <Trans>Buy Now</Trans>
            </button>
          </FlexRow.Col>
          <FlexRow.Col>
            <CustomerRated />
          </FlexRow.Col>
        </FlexRow>
      </form>
    </div>
  );
};

export default BuyForm;
