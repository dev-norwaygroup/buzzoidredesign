import { Trans } from '@lingui/macro';

const DataAutomatic = [
  {
    type: 'radios',
    name: 'automaticLikes',
    track: true,
    value: [
      {
        value: '50',
        orderCount: 17,
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $9.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: false,
        saved: false,
      },
      {
        value: '100',
        orderCount: 9,
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $19.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: true,
        popular: true,
        save: '37%',
        saved: '$4.11',
      },
      {
        value: '250',
        orderCount: 10,
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $29.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '46%',
        saved: '$11.07',
      },
      {
        value: '500',
        orderCount: 11,
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $49.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '55%',
        saved: '$30.54',
      },
      {
        value: '1000',
        orderCount: 12,
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $84.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '64%',
        saved: '$79.98',
      },
      {
        value: '1500',
        orderCount: 13,
        label: (
          <span>
            <Trans>likes per post</Trans>
            <strong>
              $114.99 / <Trans>mo</Trans>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '75%',
        saved: '$266.97',
      }
    ],
  },
];

export default DataAutomatic;
