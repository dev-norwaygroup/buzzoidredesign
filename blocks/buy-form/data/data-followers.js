import { Trans } from '@lingui/macro';

const FollowersData = [
  {
    type: 'inline-radios',
    name: 'type',
    value: [
      {
        renderValue: <Trans>High Quality</Trans>,
        value: 'High Quality',
        checked: true,
        description: (
          <Trans>
            These are Likes with profile pictures but no further uploads on
            their account. Auto-refill is enabled within the warranty.
          </Trans>
        ),
      },
      {
        renderValue: <Trans>Active Followers</Trans>,
        value: 'Active Followers',
        checked: false,
        description: (
          <Trans>
            Buzzoid is now the only website to offer active likes. That's right
            — 100% real Instagram likes, from 100% real Instagram users! Likes
            from real people really interested in your content. NO Drops!
          </Trans>
        ),
      },
    ],
  },
  {
    type: 'radios',
    name: 'followers',
    condition: {
      type: 'type',
      value: 'High Quality',
    },
    value: [
      {
        value: '100',
        orderCount: 5,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $2.97 / <strike>$3.30</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: false,
        saved: '$0.33',
      },
      {
        value: '250',
        orderCount: 112,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $4.99 / <strike>$7.43</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '23%',
        saved: '$2.44',
      },
      {
        value: '500',
        orderCount: 6,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $6.99 / <strike>$14.85</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '36%',
        saved: '$7.86',
      },
      {
        value: '1000',
        orderCount: 61,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $12.99 / <strike>$29.70</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '49%',
        saved: '$16.71',
      },
      {
        value: '2500',
        orderCount: 7,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $29.99 / <strike>$74.25</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '62%',
        saved: '$44.26',
      },
      {
        value: '5000',
        orderCount: 8,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $39.99 / <strike>$148.50</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '75%',
        saved: '$108.51',
      },
    ],
  },
  {
    type: 'radios',
    name: 'followers_active',
    condition: {
      type: 'type',
      value: 'Active Followers',
    },
    value: [
      {
        value: '500',
        orderCount: 138,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $29.99 / <strike>$15.64</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '37%',
        saved: '$4.00',
      },
      {
        value: '1000',
        orderCount: 139,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $39.99 / <strike>$15.64</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '37%',
        saved: '$25.44',
      },
      {
        value: '2500',
        orderCount: 140,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $49.99 / <strike>$15.64</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '37%',
        saved: '$74.99',
      },
      {
        value: '5000',
        orderCount: 141,
        label: (
          <span>
            <Trans>followers</Trans>
            <strong>
              $59.99 / <strike>$15.64</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '37%',
        saved: '$229.79',
      },
    ],
  },
];

export default FollowersData;
