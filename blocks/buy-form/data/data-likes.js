import { Trans } from '@lingui/macro';

const LikesData = [
  {
    type: 'inline-radios',
    name: 'type',
    value: [
      {
        renderValue: <Trans>High Quality Likes</Trans>,
        value: 'High Quality Likes',
        checked: true,
        description: (
          <Trans>
            These are Likes with profile pictures but no further uploads on
            their account. Auto-refill is enabled within the warranty.
          </Trans>
        ),
      },
      {
        renderValue: <Trans>Premium Likes</Trans>,
        value: 'Premium Likes',
        checked: false,
        description: (
          <Trans>
            Buzzoid is now the only website to offer active likes. That's right
            — 100% real Instagram likes, from 100% real Instagram users! Likes
            from real people really interested in your content. NO Drops!
          </Trans>
        ),
      },
    ],
  },
  {
    type: 'radios',
    name: 'likes',
    condition: {
      type: 'type',
      value: 'High Quality Likes',
    },
    value: [
      {
        value: '50',
        orderCount: 146,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $1.47 <strike>$1.63</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: false,
        saved: '$0.16',
      },
      {
        value: '100',
        orderCount: 147,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $2.97 <strike> $3.67</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: true,
        save: '10%',
        saved: '$0.70',
      },
      {
        value: '250',
        orderCount: 148,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $4.99 <strike>$6.93</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '28%',
        saved: '$1.94',
      },
      {
        value: '500',
        orderCount: 149,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $6.99 <strike>$11.10</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '37%',
        saved: '$4.11',
      },
      {
        value: '1000',
        orderCount: 150,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $12.99 <strike>$24.06</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '37%',
        saved: '$11.07',
      },
      {
        value: '2500',
        orderCount: 151,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $24.99 <strike>$55.53</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '55%',
        saved: '$30.54',
      },
      {
        value: '5000',
        orderCount: 152,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $44.99 <strike>$124.97</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '64%',
        saved: '$79.98',
      },
      {
        value: '10000',
        orderCount: 153,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $88.99 <strike>$355.96</strike>
            </strong>
          </span>
        ),
        visible: false,
        popular: false,
        save: '75%',
        saved: '$266.97',
      },
    ],
  },
  {
    type: 'radios',
    name: 'likes_premium',
    condition: {
      type: 'type',
      value: 'Premium Likes',
    },
    value: [
      {
        value: '50',
        orderCount: 143,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $3.49 <strike>$5.54</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: false,
        saved: '$2.05',
      },
      {
        value: '100',
        orderCount: 144,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $6.99 <strike>$12.94</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: true,
        save: '46%',
        saved: '$5.95',
      },
      {
        value: '250',
        orderCount: 145,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $12.99 <strike>$28.87</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '55%',
        saved: '$15.88',
      },
      {
        value: '500',
        orderCount: 154,
        label: (
          <span>
            <Trans>likes:</Trans>
            <strong>
              $19.99 <strike>$55.53</strike>
            </strong>
          </span>
        ),
        visible: true,
        popular: false,
        save: '64%',
        saved: '$35.54',
      },
    ],
  },
];

export default LikesData;
