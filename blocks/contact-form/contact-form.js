/**
 * External Dependencies
 */
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { Trans } from '@lingui/macro';

const schema = yup.object().shape({
  email: yup
    .string()
    .email('Invalid email')
    .required('Please enter your email'),
  name: yup.string().required('Please enter your name'),
  message: yup.string().required('Please enter a message'),
});

const ContactForm = () => {
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showFailureMessage, setShowFailureMessage] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = async (data) => {
    const { email, name, subject, orderNumber, message } = data;
    const res = await fetch('/api/sendgrid', {
      body: JSON.stringify({
        email: email,
        name: name,
        message: message,
        subject: subject ? subject : 'Empty',
        orderNumber: orderNumber ? orderNumber : 'Empty',
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    });

    const { error } = await res.json();
    if (error) {
      setShowFailureMessage(true);
      return;
    }
    setShowSuccessMessage(true);
    reset();
  };

  return (
    <div className="form contact-form">
      <form onSubmit={handleSubmit(onSubmitHandler)}>
        <div className="form__head">
          <h1><Trans>Contact Us</Trans></h1>
          <p><Trans>Ask us if you have any questions. We can help!</Trans></p>
        </div>
        <div className="form__body">
          <div className="form__row">
            <label htmlFor="name" className="form__label required">
            <Trans>Your name</Trans>
            </label>
            <input
              {...register('name')}
              type="text"
              className="form__field"
              name="name"
              id="name"
              placeholder="Your Name"
            />
            {errors.name && <p className="error">{errors.name.message}</p>}
          </div>
          <div className="form__row">
            <label htmlFor="email" className="form__label required">
            <Trans>Your e-mail</Trans>
            </label>
            <input
              {...register('email')}
              type="text"
              className="form__field"
              name="email"
              id="email"
              placeholder="example@example.com"
            />
            {errors.email && <p className="error">{errors.email.message}</p>}
          </div>
          <div className="form__row">
            <label htmlFor="order-number" className="form__label">
            <Trans>Order number</Trans>
            </label>
            <input
              {...register('order-number')}
              type="text"
              className="form__field"
              id="order-number"
              name="order-number"
              placeholder="10000123"
            />
            {errors.orderNumber && (
              <p className="error">{errors.orderNumber.message}</p>
            )}
          </div>
          <div className="form__row">
            <label htmlFor="subject" className="form__label">
            <Trans>Subject</Trans>
            </label>
            <input
              {...register('subject')}
              type="text"
              className="form__field"
              id="subject"
              name="subject"
              placeholder="Subject"
            />
            {errors.subject && (
              <p className="error">{errors.subject.message}</p>
            )}
          </div>
          <div className="form__row last">
            <label htmlFor="message" className="form__label required">
            <Trans>Message</Trans>
            </label>
            <textarea
              {...register('message')}
              id="message"
              className="form__field form__field--textarea"
              name="message"
              placeholder="Type a message..."
            />
            {errors.message && (
              <p className="error">{errors.message.message}</p>
            )}
          </div>
        </div>
        <div className="form__actions">
          <button className="btn btn--orange"><Trans>Send</Trans></button>
          {showSuccessMessage && (
            <p className="success">
              <Trans>Thank you! Your message has been delivered.</Trans>
            </p>
          )}
          {showFailureMessage && (
            <p className="error"><Trans>Service is currently unavaiable. Please try again later</Trans>.</p>
          )}
        </div>
      </form>
    </div>
  );
};

export default ContactForm;
