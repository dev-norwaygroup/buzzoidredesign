/**
 * External Dependencies
 */
import { useState, useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Trans } from '@lingui/macro';
import { useRouter } from 'next/router';

/**
 * Internal dependencies
 */

import BuyFormAltColumns from './buy-form-alt-columns';
import BuyFormAltGridRadios from './buy-form-alt-grid-radios';
import BuyFormAltSubmit from './buy-form-alt-submit';

const orderBaseUrl = process.env.NEXT_PUBLIC_ORDER_API_BASE_URL;

const BuyFormAlt = ({ fields, schema }) => {
  const [active, setActive] = useState(false);
  const [price, setPrice] = useState({});
  const [orderCount, setOrderCount] = useState();
  const closeTooltip = () => setActive(false);
  const toggleTooltip = () => setActive(!active);

  const router = useRouter();

  const handleToggleClick = () => {
    if (active) {
      setActive(false);
    }
  };

  useEffect(() => {
    if (typeof window === 'object') {
      window.addEventListener('click', handleToggleClick);
    }
    return () => window.removeEventListener('click', handleToggleClick);
  });

  const defaultValues = fields.reduce((acc, val) => {
    if (val.value.filter((item) => (item.checked ? true : false))) {
      return {
        ...acc,
        [val.name]: val.value.filter((item) => item.checked)[0]?.value,
      };
    }
    return acc;
  }, {});
  const {
    control,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    defaultValues,
    resolver: yupResolver(schema),
  });

  const handlePrice = (data, name, count) => {
    setPrice((old) => ({
      ...old,
      [name]: data,
    }));
    setOrderCount(count);
  };
  const onSubmit = (data) => {
    let newData = data;
    let packageName = '';
    const url = `${orderBaseUrl}/${orderCount}`;

    const conditionalField = fields.find((item) => !item.condition);
    if (conditionalField) {
      const conditionalFieldName = conditionalField.name;
      const conditionalFieldValue = data[conditionalFieldName];
      const fieldToRemove = fields.find(
        (item) =>
          item.condition && item.condition.value != conditionalFieldValue
      );
      const fieldToRemoveName = fieldToRemove.name;
      delete newData[fieldToRemoveName];
      packageName = Object.keys(newData)[1];
    }

    if (!newData[packageName]) {
      newData[packageName] = price[packageName].value;
    }

    router.push(url, url);
    return newData;
  };

  return (
    <div className="form buy-form-alt">
      <form onSubmit={handleSubmit(onSubmit)}>
        {fields.map((item) => {
          switch (item.type) {
            case 'inline-radios':
              return (
                <Controller
                  key={item.name}
                  render={({ field }) => (
                    <div className="radio-columns-holder">
                      <div className="inline-radio-recommended">
                        <Trans>Recommended</Trans>
                      </div>
                      <BuyFormAltColumns
                        active={active}
                        toggleTooltip={toggleTooltip}
                        fields={item.value}
                        field={field}
                        name={item.name}
                        key={item.type}
                        onChange={(e) => {
                          field.onChange(e);
                          closeTooltip();
                        }}
                      />
                      {errors[item.name]?.message && (
                        <p className="error">{errors[item.name].message}</p>
                      )}
                    </div>
                  )}
                  control={control}
                  name={item.name}
                />
              );
            case 'radios':
              if (item.condition) {
                const typeWatch = watch(item.condition.type);
                if (typeWatch != item.condition.value) {
                  return;
                }
              }
              return (
                <Controller
                  key={item.name}
                  render={({ field }) => (
                    <>
                      <div className="grid-radios-holder">
                        <BuyFormAltSubmit price={price} name={item.name} />
                        {errors[item.name]?.message && (
                          <p className="error">{errors[item.name].message}</p>
                        )}
                        <BuyFormAltGridRadios
                          fields={item.value}
                          value={field.value}
                          renderName={item.renderName}
                          name={item.name}
                          key={item.type}
                          handlePrice={handlePrice}
                          onChange={field.onChange}
                          setOrderCount={setOrderCount}
                        />

                      </div>
                    </>
                  )}
                  control={control}
                  name={item.name}
                />
              );
            default:
              return null;
          }
        })}
      </form>
    </div>
  );
};

export default BuyFormAlt;
