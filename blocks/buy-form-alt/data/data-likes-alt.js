import { Trans } from '@lingui/macro';

const LikesAltData = [
  {
    type: 'inline-radios',
    name: 'type',
    value: [
      {
        renderValue: <Trans>High Quality Likes</Trans>,
        value: 'High Quality Likes',
        tooltip: false,
        checked: true,
        color: 'orange',
        list: [
          {
            isToggler: true,
            value: (
              <>
                <strong><Trans>Real </Trans></strong> <Trans>likes from </Trans><strong>{' '}<Trans>Real</Trans></strong> <Trans>people</Trans>{' '}
              </>
            ),
          },
          {
            value: (
              <>
                <strong><Trans>Guaranteed Delivery</Trans></strong>
                <div>(<Trans>gradual or instant</Trans>)</div>
              </>
            ),
          },
          {
            value: (
              <>
                <Trans>Option to </Trans>{' '}<strong><Trans>split likes</Trans></strong> <Trans>on multiple pictures</Trans>
              </>
            ),
          },
          {
            value: <Trans>Includes video views</Trans>,
          },
          {
            value: (
              <>
                <strong><Trans>No password</Trans></strong>
                <br /> <Trans>required</Trans>
              </>
            ),
          },
          {
            value: (
              <>
                <strong>24/7</strong> <Trans>support</Trans>
              </>
            ),
          },
        ],
        description: (
          <Trans>
            These are Likes with profile pictures but no further uploads on
            their account. Auto-refill is enabled within the warranty.
          </Trans>
        ),
      },
      {
        renderValue: <Trans>Premium Likes</Trans>,
        value: 'Premium Likes',
        checked: false,
        tooltip: true,
        color: 'blue',
        list: [
          {
            isToggler: true,
            value: (
              <>
                <strong><Trans>Real</Trans></strong> <Trans>likes from</Trans> <strong><Trans>Active</Trans></strong>{' '}
                profiles
                <br />
              </>
            ),
          },
          {
            value: (
              <>
                <strong><Trans>Guaranteed Delivery</Trans></strong>
                <div>(<Trans>gradual or instant</Trans>)</div>
              </>
            ),
          },
          {
            value: (
              <>
                <Trans>Option to </Trans>{' '}<strong><Trans>split likes</Trans></strong> <Trans>on multiple pictures</Trans>
              </>
            ),
          },
          {
            value: <Trans>Includes video views</Trans>,
          },
          {
            value: (
              <>
                <strong><Trans>No password</Trans></strong>
                <br /> <Trans>required</Trans>
              </>
            ),
          },
          {
            value: (
              <>
                <strong>24/7</strong> <Trans>support</Trans>
              </>
            ),
          },
          {
            highlighted: true,
            value: <Trans>Increased change to reach explore page</Trans>,
          },
        ],
        description: (
          <Trans>
            Buzzoid is now the only website to offer active likes. That's right
            — 100% real Instagram likes, from 100% real Instagram users! Likes
            from real people really interested in your content. NO Drops!
          </Trans>
        ),
      },
    ],
  },
  {
    type: 'radios',
    name: 'likes',
    renderName: <Trans>likes</Trans>,
    condition: {
      type: 'type',
      value: 'High Quality Likes',
    },
    value: [
      {
        defaultChecked: true,
        value: '50',
        orderCount: 146,
        price: '1.47',
        old: '$1.63',
        save: false,
        saved: '$0.16',
      },
      {
        value: '100',
        orderCount: 147,
        price: '2.97',
        old: '$3.67',
        save: '19%',
        saved: '$0.70',
      },
      {
        value: '250',
        orderCount: 148,
        price: '4.99',
        old: '$6.93',
        save: '28%',
        saved: '$1.94',
      },
      {
        value: '500',
        orderCount: 149,
        price: '6.99',
        old: '$11.10',
        save: '37%',
        saved: '$4.11',
      },
      {
        value: '1000',
        orderCount: 150,
        price: '12.99',
        old: '$24.06',
        save: '46%',
        saved: '$11.07',
      },
      {
        value: '2500',
        orderCount: 151,
        price: '24.99',
        old: '$55.53',
        save: '55%',
        saved: '$30.54',
      },
      {
        value: '5000',
        orderCount: 152,
        price: '44.99',
        old: '$124.97',
        save: '64%',
        saved: '$79.98',
      },
      {
        value: '10000',
        orderCount: 153,
        price: '88.99',
        old: '$355.96',
        save: '75%',
        saved: '$266.97',
      },
    ],
  },
  {
    type: 'radios',
    name: 'likes_premium',
    renderName: <Trans>likes</Trans>,
    condition: {
      type: 'type',
      value: 'Premium Likes',
    },
    value: [
      {
        defaultChecked: true,
        value: '50',
        orderCount: 143,
        price: '3.49',
        old: '$5.54',
        save: false,
        saved: '$2.05',
      },
      {
        value: '100',
        orderCount: 144,
        price: '6.99',
        old: '$12.94',
        save: '46%',
        saved: '$5.95',
      },
      {
        value: '250',
        orderCount: 145,
        price: '12.99',
        old: '$28.87',
        save: '55%',
        saved: '$15.88',
      },
      {
        value: '500',
        orderCount: 154,
        price: '19.99',
        old: '$55.53',
        save: '64%',
        saved: '$35.54',
      }
    ],
  },
];

export default LikesAltData;
