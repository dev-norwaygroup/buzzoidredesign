import { Trans } from '@lingui/macro';

const FollowersAltData = [
  {
    type: 'inline-radios',
    name: 'type',
    value: [
      {
        renderValue: <Trans>High Quality Followers</Trans>,
        value: 'High Quality Followers',
        tooltip: false,
        checked: true,
        color: 'orange',
        list: [
          {
            value: (
              <>
                <strong><Trans>High Quality</Trans></strong>
                <br /> <Trans>Followers</Trans>
              </>
            ),
          },
          {
            value: (
              <>
                <strong><Trans>Fast </Trans></strong> <Trans>Delivery</Trans>
              </>
            ),
          },
          {
            value: (
              <>
                <strong><Trans>No password</Trans></strong>
                <br /> <Trans>required</Trans>
              </>

            ),
          },
          {
            value: (
              <><strong>24/7</strong> <Trans>support </Trans></>
            ),
          },
        ],
        description: (
          <Trans>
            These are Likes with profile pictures but no further uploads on
            their account. Auto-refill is enabled within the warranty.
          </Trans>
        ),
      },
      {
        renderValue: <Trans>Active Followers</Trans>,
        value: 'Active Followers',
        checked: false,
        tooltip: true,
        color: 'blue',
        list: [
          {
            value: (
              <>
                <strong><Trans>Real Active </Trans></strong> <Trans>Followers</Trans>
              </>
            ),
          },
          {
            value: (
              <>
                <Trans>Guaranteed </Trans><strong>{' '}<Trans>Instant Delivery</Trans></strong>
              </>
            ),
          },
          {
            value: (
              <>
                <strong><Trans>No password</Trans></strong>
                <br /> <Trans>required</Trans>
              </>
            ),
          },
          {
            value: (
              <>
                <strong><Trans>30 day</Trans></strong>{' '}<Trans>refills</Trans>
              </>
            ),
          },
          {
            value: (
              <>
                <strong>24/7 </strong> <Trans>support</Trans>
              </>
            ),
          },
        ],
        description: (
          <Trans>
            Buzzoid is now the only website to offer active likes. That&apos;s
            right — 100% real Instagram likes, from 100% real Instagram users!
            Likes from real people really interested in your content. NO Drops!
          </Trans>
        ),
      },
    ],
  },
  {
    type: 'radios',
    name: 'followers',
    renderName: <Trans>followers</Trans>,
    condition: {
      type: 'type',
      value: 'High Quality Followers',
    },
    value: [
      {
        defaultChecked: true,
        value: '100',
        orderCount: 5,
        price: '2.97',
        old: '$3.30',
        save: false,
        saved: '$0.33',
      },
      {
        value: '250',
        orderCount: 112,
        price: '4.99',
        old: '$7.43',
        save: '23%',
        saved: '$2.44',
      },
      {
        value: '500',
        orderCount: 6,
        price: '6.99',
        old: '$14.85',
        save: '36%',
        saved: '$7.86',
      },
      {
        value: '1000',
        orderCount: 61,
        price: '12.99',
        old: '$29.70',
        save: '49%',
        saved: '$16.71',
      },
      {
        value: '2500',
        orderCount: 7,
        price: '29.99',
        old: '$74.25',
        save: '62%',
        saved: '$44.26',
      },
      {
        value: '5000',
        orderCount: 8,
        price: '39.99',
        old: '$148.50',
        save: '75%',
        saved: '$108.51',
      },
    ],
  },
  {
    type: 'radios',
    name: 'followers_active',
    renderName: <Trans>followers</Trans>,
    condition: {
      type: 'type',
      value: 'Active Followers',
    },
    value: [
      {
        defaultChecked: true,
        orderCount: 138,
        value: '500',
        price: '11.99',
        old: '$15.99',
        save: false,
        saved: '$4.00',
      },
      {
        value: '1000',
        orderCount: 139,
        price: '19.99',
        old: '$45.43',
        save: '49%',
        saved: '$25.44',
      },
      {
        value: '2500',
        orderCount: 140,
        price: '49.99',
        old: '$124.98',
        save: '62%',
        saved: '$74.99',
      },
      {
        value: '5000',
        orderCount: 141,
        price: '84.99',
        old: '$314.78',
        save: '62%',
        saved: '$229.79',
      },
    ],
  },
];

export default FollowersAltData;
