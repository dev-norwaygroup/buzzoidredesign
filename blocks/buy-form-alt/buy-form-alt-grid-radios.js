/**
 * External Dependencies
 */
import { Trans } from '@lingui/macro';
import { useEffect } from 'react';

const BuyFormAltGridRadios = ({
  fields,
  name,
  renderName,
  handlePrice,
  setOrderCount,
  onChange,
  value,
}) => {
  useEffect( () => {
    if (!value) {
      handlePrice(fields[0], name, fields[0].orderCount);
    }
  }, [fields]);

  return (
    <div className="grid-radios">
      {fields.map((field, index) => {
        const { save } = field;
        return (
          <div className="grid-radios__item" key={index}>
            <input
              type="radio"
              name={name}
              value={field.value}
              id={`radio-${field.value}`}
              checked={value ? field.value === value : !!field.defaultChecked}
              onChange={(e) => {
                handlePrice(field, name);
                onChange(e);
                setOrderCount(field.orderCount)
              }}
            />
            <label htmlFor={`radio-${field.value}`}>
              {field.value} {!save && <span>{renderName}</span>}
            </label>
            {save && (
              <div className="grid-radios__item-save">
                {save} <Trans>off</Trans>
              </div>
            )}
            <div className="grid-radios__item-bg"></div>
          </div>
        );
      })}
    </div>
  );
};

export default BuyFormAltGridRadios;
