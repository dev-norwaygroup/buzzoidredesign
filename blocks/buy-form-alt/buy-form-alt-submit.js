/**
 * External dependencies
 */
import { Trans } from '@lingui/macro';

/**
 * Internal dependencies
 */
import FlexRow from '@/components/flex-row/flex-row';
import IconCheckSaved from '@/svg/icon-check-saved.svg';

const BuyFormAltSubmit = ({ price, name }) => {
  return (
    <div className="flex-row align-center buy-form-alt__actions">
      <div className="flex-col">
        {price[name] && (
          <div className="buy-form-alt__price">
            <h4>
              <sup>$</sup>
              {price[name].price}
              <sup className="strike">{price[name].old}</sup>
            </h4>
            {price[name].saved && (
              <FlexRow className="align-center">
                <FlexRow.Col>
                  <IconCheckSaved />
                </FlexRow.Col>
                <FlexRow.Col>
                  <span className="buy-form-alt__saved">
                    <Trans>You Saved</Trans>{' '}
                    <strong>{price[name].saved}</strong>
                  </span>
                </FlexRow.Col>
              </FlexRow>
            )}
          </div>
        )}
      </div>
      <div className="flex-col">
        <button type="submit" className="btn btn--orange">
          <Trans>Buy Now</Trans>
        </button>
      </div>
    </div>
  );
};

export default BuyFormAltSubmit;
