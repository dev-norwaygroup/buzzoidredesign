/**
 * External Dependencies
 */
import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

/**
 * Internal Dependencies
 */
import StarsRate from '../../components/stars-rate/stars-rate';
import { Trans } from '@lingui/macro';

const schema = yup.object().shape({
    email: yup.string().email("Invalid email").required("Please enter your email"),
    name: yup.string().required("Please enter your name"),
    review: yup.string().required("Please enter a message"),
});

const ReviewForm = ({ isOpen }) => {
    const [rating, setRating] = useState(0);
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);

    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        resolver: yupResolver(schema)
    });

    useEffect(() => {
        reset();
        setRating(0);
        setShowSuccessMessage(false);
    }, [isOpen])

    const onSubmitHandler = (data) => {
        const { email, name, subject, orderNumber, message } = data;
        reset();
        setRating(0);
        setShowSuccessMessage(true);
    };

    const onRating = (index) => setRating(index);

    return (
        <div className="form review-form" >
            <form onSubmit={handleSubmit(onSubmitHandler)}>
                <div className="form__body">
                    <div className="form__group">
                        <div className="form__row">
                            <label htmlFor="name" className="form__label"><Trans>Your name</Trans></label>
                            <input
                                {...register('name')}
                                type="text"
                                className="form__field"
                                name="name"
                                id="name"
                                placeholder="Your Name"
                            />
                            {errors.name && <p className="error">{errors.name.message}</p>}
                        </div>
                        <div className="form__row">
                            <label htmlFor="email" className="form__label"><Trans>Your e-mail</Trans></label>
                            <input
                                {...register('email')}
                                type="text"
                                className="form__field"
                                name="email"
                                id="email"
                                placeholder="example@example.com"
                            />
                            {errors.email && <p className="error">{errors.email.message}</p>}
                        </div>

                        <div className="form__rating">
                            <span className="form__label"><Trans>Rating</Trans></span>
                            <StarsRate count={5} onRating={onRating} rating={rating} />
                        </div>
                    </div>
                    <div className="form__group">
                        <div className="form__row">
                            <label htmlFor="review" className="form__label"><Trans>Review</Trans></label>
                            <textarea
                                {...register('review')}
                                id="review"
                                className="form__field form__field--textarea"
                                name="review"
                                placeholder="Your review"
                            />
                            {errors.review && <p className="error">{errors.review.message}</p>}
                        </div>
                        <div className="form__actions">
                            <button className="btn btn--orange"><Trans>Submit Review</Trans></button>
                        </div>
                        {showSuccessMessage && (
                            <p className="success">
                                Thank you! Your review has been delivered.
                            </p>
                        )}
                    </div>
                </div>
            </form>
        </div>
    )
}

export default ReviewForm;