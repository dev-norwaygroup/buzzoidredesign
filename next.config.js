module.exports = {
  reactStrictMode: true,
  trailingSlash: true,
  i18n: {
    locales: ['en', 'fr', 'de', 'nl', 'pt'],
    defaultLocale: 'en',
    localeDetection: false,
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    return config;
  },
  env: {
    BASE_URL: process.env.BASE_URL,
  }
};
